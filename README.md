# SV3 Cloud Connector

This is the consolidated SV3 Cloud Connector project for all connectors.

| Integration | Status        | Updated   | Notes      |
| ----------- | ------        | --------- | ---------- |
| CCURE9000   | Complete      | 2/21/2018 | |
| Genetec     | not merged    | 2/21/2018 | |
| Vidsys      | partial merge | 2/21/2018 | To be merged from git@bitbucket.org:buildingintelligence/sv3_cloud_connectors.git Vidsys directory |
| Amag        | not merged    | 2/21/2018 | Currently implemented in .Net/C#, to be reimpemented in Node.js using [mssql](https://www.npmjs.com/package/mssql) module and [tedious](https://www.npmjs.com/package/tedious) driver |
| ODBC        | not merged    | | Not yet created.  Generic ODBC import/export module |
| CCURE800    | not merged    | | |
| Lenel       | not merged    | | |
| ELSAG       | not merged    | | lpr_broker merge |
| Inex Zamir  | not merged    | | lpr_broker merge |
| Viper LPR   | not merged    | | lpr_broker merge |

This is the cloud connector project specific to the CCURE9000 integration.  The project includes:

* webservice: the broker and web service client api to Victor Web Services.
* Windows EXE/MSI binary distribution build


# Development

## Starting the service

	$ npm start

## Running Tests

	$ npm test

or 

	$ gulp test

## Linting

	$ gulp lint

## TODO list

Generates TODO.md listing all TODO, FIXMEs, etc. from code comments:

	$ gulp todo

## Packaging

Run this command on the OS/Platform/Arch that you wish to create (read: run this on Windows)

	$ gulp pkg

## Build Setup.exe Installer

This is only works on Windows where Inno Setup is installed.

	$ gulp inno

If it fails, run the script manually in the Inno Script Studio to troubleshoot. 
May require updating paths specific to your system.  Usually this fails when Signtool
has not been configured or is in a different path.

### Signtool command

```
"C:\Program Files (x86)\Windows Kits\10\App Certification Kit\signtool.exe" sign /f "F:\Building Intelligence Code Signing.pfx" /t http://timestamp.comodoca.com/authenticode /p "PASSWORD" $f
```

# Structure

- ./bin - executables
- ./build - destination where pkg and Inno Setup save compiled application components
- ./docs - a place for all the documentation (future Gitbook location, 3rd party APIs, etc)
- ./lib - all the code
- ./logs - default log location
- ./pki - load/save certificates from here
- ./public - location of experimental web front-end configuration interface
- ./scripts - a place for scripts (for development purposes)

- README.md - this file. An introduction to the project
- CHANGES.md - a log of changes for each version, tracking features added/removed and bugfixes.
- TODO.md - tracking all the TODO comments.  use `gulp todo` to update

- ccure.js - original (depricated) client library for CCURE9000 Web Service API.  Do not use this.
- config.example.json - example configuration file. keep this up-to-date with (default) config options
- config.json - the actual config file for an installation.  this is ignored by git.  Do not check one in.
- db.js - node-persist abstraction for depricated/broken employee temp card issuance feature
- sv3cc.exe - WinSW executable for creating/managing the SV3CC Windows Service
- sv3cc.xml - WinSW configuration file
- sv3cc-setup.iss - Inno Setup configuration/build file
- server.js - main executable; runs all other components, logs in, and starts processing commands from SV3


# TODO

See TODO.md for all the TODOs.  These are some high-level ones:

* Move server.js into ./bin/
* Fix configuration and runtime server.js
* Implement Ccure::Client.update() method
* Modify Ccure::Service.save() to support updates
* Implement Ccure9kObject.changes() and related calls such that Service.save() only sends changes.
* Complete refactor of CCURE client
    * fetch_directory command (and others) should use new library
    * Support for paginated queries (currently limited to single 10,000 page result)
* Abstract complex actions from ccure.js to Service wrapper
* Complete test coverage
* Add test spies, stubs and mocks with [sinon](http://sinonjs.org/)
* Add ability to run tests without actual CCURE instance
	* Use [nock](https://www.npmjs.com/package/nock) to simulate expected Victor response.


## Known Issues

### v2.4.3

* RESOLVED: Visitor/host search scoping issue
* If a valid host is not found in CCURE, the visitor is added without a credential or clearance

### v2.4.2

* Visitor and Host searches are not scoped by PersonnelType.  Visitor search by Full Name / Email may return Host with matching name/email
* RESOLVED: Max Credential export results

### v2.4.1

* Max results returned from requests using the old/depricated CCURE library changed to 12,000.  To be resolved in a future release.
* Max Credential results (from new export) limited to 10.

### v2.4.0

* Requests for fetch_directory and other commands that use the old/depricated CCURE library may fail silently when the session expires.  This is resolved in v2.4.1.
* Requests for Personnel and other objects using the old/depricated CCURE library (including fetch_directory command), are limited to 10,000 results.  For systems requiring directory sync and have greater than 10,000 Personnel, there will be some missing in the sync.
* Resolved: Authentication Loop issue.

### v2.3.1

* Authentication loop issue still exists, restarting connector may resolve (could be stale session ID and/or token)

### v2.3.0

* The authentication loop issue may still be unresolved. Long term testing has not been completed.
* Merging the improvements from the Vidsys fork is still incomplete.
* Configuration continues to use the unencrypted config.json file, and passwords are logged in DEBUG.

### v2.0.1

* Configuration is manual. Drop a valid JSON config file in the working directory named [license code].json
* Card reading feature is disabled
* Visits are not checked-in or checked-out, they are only created.
* Visitor photos are not yet transfered.
* Memory profiling has not been done


### v1.5.3

* GUI configuration feature is broken:
	 * shows only blank page
	 * does not generate UUID in config.json
	 * How is this suppose to work?
* SmartID field missing and required for CCURE v2.60
* Escort flag is hard-coded to UNESCORTED visitor. Some customer(s) want ESCORTED visitors.
* Credential deletion fails silently.  Adding existing credential errors with NonUniqueCHUID error on some systems and not others.  WHY?
* Missing scans (default 2-second window too short. 6-10 second lag on test server)
  *Memory leak: Memory usage on Windows grows by ~100kb/s*


## Additional Use Cases

1. Check-in Visitor but cancel the card read
		1. This presently causes Shortpath to not send the visitor data to MQ
		1. This SHOULD still send visitor data to MQ -> CCURE and create the Personnel and Visit records
		1. The Visitor can be assigned a card at any later time.


# Roadmap

* Encrypted configuration file (merge from Vidsys fork)
* Remote logging (AMQP topic stream)
* Fix configuration and licensing issue.  Things that may help with that:
  * [opn](https://github.com/sindresorhus/opn) can cause a browser to open (to the config/license page/server)
  * [node-forge](https://www.npmjs.com/package/node-forge) native TLS (pure JS) 
  * [pem](https://www.npmjs.com/package/pem) generate client x509 certificates (pure JS, no deps)
* Restore Journal/card reading (improved polling)
* Support for HTTPS connection to CCURE web service
* Visitor check-in via card read (may require push/signalr)

## v3.0.0

* Finally fully replace ccure.js
* Add support for push notification via signalr
* Consolidate support for:
  * HTTP proxy and HTTP CONNECT proxy
     * Support for using/collecting system CA certificates
  * VidSys push support
  * Print Service



