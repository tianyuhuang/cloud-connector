### TODOs
| Filename | line # | TODO
|:------|:------:|:------
| logger.js | 55 | Add file out method for debugging PowerShell and other IO operations
| ccure\Ccure9kObject.js | 16 | Add support for deep freeze (deep clone object and recursively freeze each property of type object)
| ccure\Ccure9kObject.js | 38 | return an object whose keys are properties that have changed, and the values are [before, after] tuples
| ccure\Clearance.spec.js | 28 | add reference to client
| ccure\Client.js | 63 | Make Login return Promise!
| ccure\Client.js | 151 | Refactor API into namespaced modules like Twilio node library
| ccure\Client.js | 382 | Client will encounter a 404 error when there are no results.  Fix this by returning [] instead.
| ccure\Client.js | 400 | reject() should return Error, not string
| ccure\Client.js | 507 | reject() should return Error, not string
| ccure\Client.spec.js | 63 | test for various error returns.  POST /Api/Objects/Persist can return: Error or non-200 status codes.
| ccure\Client.spec.js | 120 | update with use of new random();
| ccure\Credential.spec.js | 28 | add reference to client
| ccure\Criteria.js | 37 | Use rest/arguments to support any number of criteria sets
| ccure\Criteria.js | 38 | Complete JSDOC params and examples
| ccure\Personnel.js | 46 | inherit from Ccure9kObject
| ccure\Personnel.js | 64 | add Cards getter with WeakMap private data
| ccure\Personnel.js | 65 | override serialize() method to support Children.  See Visit for example.
| ccure\Personnel.spec.js | 51 | add reference to client
| ccure\PersonnelType.js | 32 | inherit from Ccure9kObject
| ccure\Service.js | 174 | work on nomenclature for #findFirstMatch() and #findMultiple(); 'priorities' should maybe be order, combinations? Ugh.
| ccure\Service.js | 252 | This is memory heavy. Revise to be more efficient.  Profile to see garbage collection performance
| ccure\Service.js | 253 | Change to stream interface
| ccure\Service.js | 296 | refactor find operation to return type expected.
| ccure\Service.js | 386 | Should PartitionID be explicitly specified here?
| ccure\Service.js | 394 | Handle non-unique email error; remove email attribute and try again.
| ccure\Service.js | 395 | Add support for visitor photo!
| ccure\Service.js | 436 | handle case where shortpathVisitObj is missing Visitor UUID and host information. (older versions)
| ccure\Service.js | 454 | Sanitize host matching!!!!
| ccure\Service.js | 474 | Don't delete card, just reassign it (when updating is supported)
| ccure\Service.spec.js | 281 | break this into multiple tests
| ccure\Service.spec.js | 333 | Implement tests for #assign that creates and assigns a credential
| ccure\Service.spec.js | 389 | uncomment this line
| ccure\Service.spec.js | 394 | complete this test.
| ccure\Service.spec.js | 500 | set config:ccure:visitor:match order explicitly here to:
| ccure\Service.spec.js | 518 | set config:ccure:visitor:match order explicitly here to:
| ccure\Service.spec.js | 813 | explicitly define config:ccure:visitor:match order to ensure Name priority
| ccure\Service.spec.js | 814 | explicitly define config:ccure:host:match order to ensure EmailAddress priority
| ccure\Visit.spec.js | 48 | add reference to client
| ccure\Visit.spec.js | 108 | figure out why this gets called before results or people arrays are populated.
| sv3\AMQPClient.js | 114 | Move to callback, only called when command is completed and requeue if necessary.
| server-ccure.js | 70 | connect independently of CCURE login; hold requests until successful login
| server-ccure.js | 76 | begin polling card reads from Journal
| server-ccure.js | 160 | check to see if this is a temp badge for employee.  If yes, use old ccure.js library.
| server-ccure.js | 161 | implement temp badge issuance in new CcureService
| server-ccure.js | 162 | Message should be transactional: only confirm if checkin is successful!
| server-ccure.js | 374 | catch requests to shutdown and exit gracefully with a goodbye
| victor.js | 6 | Why is it necessary to recycle victor?  Under what conditions must this be done?