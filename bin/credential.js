/**
 * Credential
 * 
 * Intended to be a simple CLI program to find, delete, and create a credential,
 * and assign it to a Personnel record.
 *
 * Created 2/16/2018 in an effort to troubleshoot the CCURE connector
 *
 */

const nconf = require('../lib/config.js');
const moment = require('moment');

const log = require('../lib/logger.js');
const CcureClient = require('../lib/ccure/Client.js');
const CcureService = require('../lib/ccure/Service.js');
const Credential = require('../lib/ccure/Credential.js');

const client = new CcureClient(nconf.get("ccure:connection"));
const service = new CcureService(client);



client.login(function(err) {
	if (err) {
		log.error('Login failure.  Exiting.');
		console.log("CCURE9000 Login Failure.  Exiting.");
		process.exit(1);
	} else {
		run()
	}
});

const VISITOR_PERSONNEL_ID = 29498;  //Anelise Tatum
const CARD_NUMBER = 51807;

function run() {
	service.find(Credential, {CardNumber: CARD_NUMBER})
	.then(cards => {
		log.info({cards}, `Cards FOUND`); //Note: cards is an array and may be empty.
		return Promise.all(cards.map(c => { return service.destroy(c) }));
	})
	.then(() => {
		card = new Credential({ 
			CardNumber: CARD_NUMBER,
			PersonnelID: VISITOR_PERSONNEL_ID,
			ActivationDateTime: moment().startOf('day').format(Credential.FORMAT_DATETIME),
			ExpirationDateTime: moment().endOf('day').format(Credential.FORMAT_DATETIME)
		});
		return service.save(card);
	})
	.then(card => {
		log.info({card}, "Card Saved!");
	})
	.catch(err => {
		log.error({err}, "Unexpected Error");
	});
}