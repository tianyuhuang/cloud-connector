var http = require('http');
var _ = require('underscore'); //TODO: replace with lodash (if this module is still in use)
var querystring = require("querystring");
var db = require('./db.js');
var log = require('./lib/logger.js').child({module: "CCURE"});

var CCURE = function(config){
	var self = this;
	var config = config;
	self.sessionId = null;
	self.authToken = null;
	self.defaultPartition = null;

	// consts
	self.consts = {};
	self.consts.ESCORTOPTION = {
		NONE: 0, // host who can not schedule visitor
		UNESCORTED_VISITOR: 1, // visitor but require escort
		ESCORTED_VISITOR: 2, // visitor do not require escort
		ESCORT: 3 // host who can schedule and escort visitor
	};

	self.setConfig = function(_config){
		config = _config;
	}

	self.applyHost = function(options){
		options.host = config.host;
		// options.path = encodeURI(options.path);
	}

	self.applyAuth = function(options){
		if(self.authToken){
			options.path += "&token=" + self.authToken; 
		}
		if(self.sessionId){
			options.headers = options.headers || {};
			options.headers['session-id'] = self.sessionId; 
		}
		if(options.headers && !options.headers['Content-Type']){
			options.headers['Content-Type'] = "application/x-www-form-urlencoded";
		}
	};

	self.applyPagination = function(options){
		var pageNumber = options.pageNumber || 1;
		var pageSize = options.pageSize || 10;
		options.path += "&pageNumber=" + pageNumber + "&pageSize=" + pageSize; 
	};

	self.applyFilter = function(options){
		self.applyHost(options);
		if(!options.noAuth){
			self.applyAuth(options);
		}
		if(!options.noPatination){
			self.applyPagination(options);
		}

		options.url = options.host + options.path;
	};

	self.getRequest = function(options){
		self.applyFilter(options);

		var opts = {
			host: options.host,
			path: options.path,
			method: "GET",
			headers: options.headers
		}

		var req = http.request(opts, function(res) {
			res.setEncoding('utf8');
			var fullResp = "";
			res.on('data', function (chunk) {
				fullResp += chunk;
			});
			res.on('end', function(){
				options.callback(null, res, fullResp);
			});
		});
		req.on('error', function(err) {
			options.callback(err, err, null);
		});

		req.end();
		return req;
	};

	self.postRequest = function(options){
		self.applyFilter(options);
		
		var data = options.body || "";
		var opts = {
			host: options.host,
			path: options.path,
			method: "POST",
			headers: options.headers
		}
		// log.debug(opts);
		var req = http.request(opts, function(res) {
			res.setEncoding('utf8');
			var fullResp = "";
			res.on('data', function (chunk) {
				fullResp += chunk;
			});
			res.on('end', function(){
				options.callback(null, res, fullResp);
			});
		});
		req.on('error', function(err) {
			options.callback(err, err, null);
		});

		req.write(data);
		req.end();
		return req;
	};

	self.putRequest = function(options){
		self.applyFilter(options);
		
		var data = options.body || "";
		var opts = {
			host: options.host,
			path: options.path,
			method: "PUT",
			headers: options.headers
		}
		// log.debug(opts);
		var req = http.request(opts, function(res) {
			res.setEncoding('utf8');
			var fullResp = "";
			res.on('data', function (chunk) {
				fullResp += chunk;
			});
			res.on('end', function(){
				options.callback(null, res, fullResp);
			});
		});
		req.on('error', function(err) {
			options.callback(err, err, null);
		});

		req.write(data);
		req.end();
		return req;
	};

	self.deleteRequest = function(options){
		self.applyFilter(options);
		var opts = {
			host: options.host,
			path: options.path,
			method: "DELETE",
			headers: options.headers
		}
		var req = http.request(opts, function(res) {
			res.setEncoding('utf8');
			var fullResp = "";
			res.on('data', function (chunk) {
				fullResp += chunk;
			});
			res.on('end', function(){
				options.callback(null, res, fullResp);
			});
		});
		req.on('error', function(err) {
			options.callback(err, err, null);
		});

		req.end();
		return req;
	};

	// opts= {onSuccess, onError}
	self.login = function(opts={}){
		var options = {
			path: [
				"/victorWebService/api/Authenticate/Login?userName=",
				encodeURIComponent(config.username),
				"&password=",
				encodeURIComponent(config.password), //TODO: is this the correct encoding?
				"&clientName=SV3ClientLibraryV1"
			].join(''),
			noAuth: true
		};

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				var authToken = body.toString();
				authToken = authToken.replace(/\"/g,'');
				self.authToken = authToken;

				var sessionId = res.headers['session-id'];
				self.sessionId = sessionId;

				log.debug({authToken: authToken, sessionId: sessionId}, 'logged in');
				
				if(self.hasDefaultPartition()){
					if(opts.onSuccess){
						opts.onSuccess({sessionId: sessionId, authToken: authToken}, body, res);
					}
				}
				else{
					self.setDefaultPartition({
						onSuccess: function(){
							if(opts.onSuccess){
								opts.onSuccess({sessionId: sessionId, authToken: authToken}, body, res);
							}
						}
					})
				}
			} else {
				log.error({res:res, err:err, body:body}, "Login failed.");
				if(opts.onError) opts.onError(body, res, err);
			}
		};
		self.getRequest(options);
	};

	self.isLoggedIn = function(){
		if(self.sessionId && authToken){
			return true;
		}
		return false;
	};

	// opts= {onSuccess}
	self.setDefaultPartition = function(opts){
		self.findPartitions({
			defaultRecord: true,
			onSuccess: function(partitions){
				var defaultPartition = partitions[0];
				if(defaultPartition){
					self.defaultPartition = defaultPartition;
					log.debug("Default partition set", self.defaultPartition);
					opts.onSuccess(opts.onSuccess);
				}
			},
			onError: function(){
				process.exit(1);
			}
		})
	};

	//
	self.hasDefaultPartition = function(){
		if(self.defaultPartition && self.defaultPartition.id){
			return true;
		}
		return false;
	}

	// opts = {defaultRecord}
	self.findPartitions = function(opts){
		var defaultRecord = opts.defaultRecord ? 1 : 0;
		var formData = "";
		formData += "TypeFullName=" + "Partition";
		formData += "&WhereClause=" + "DefaultRecord="+defaultRecord+"";

		var formDataEndoced = encodeURI(formData);

		var options = {
			path: "/victorWebService/api/Objects/GetAllWithCriteria?"+formDataEndoced,
			accept: "application/json",
			pageSize: 10000
			// body: formDataEndoced
		}

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							guid: i.GUID,
							name: i.Name,
							defaultRecord: i.DefaultRecord,
							shared: i.Shared
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(isEmptyResult(body)){
					opts.onSuccess([], res);
				}
				else{
					if(opts.onError) opts.onError(body, res, err);
				}
			}
		};
		self.getRequest(options);
	};

	// opts = {onSuccess, onError}
	self.findPersonnelGroups = function(opts){
		var options = {
			accept: "application/json",
			pageSize: 10000,
			path: "/victorWebService/api/Objects/GetAllWithCriteria?&TypeFullName=SoftwareHouse.CrossFire.Common.Objects.Group&DisplayProperties%5B%5D=&WhereClause=GroupType%3D%27SoftwareHouse.NextGen.Common.SecurityObjects.Personnel%27&Arguments%5B%5D="
		}
		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							guid: i.GUID,
							name: i.Name
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(isEmptyResult(body)){
					opts.onSuccess([], res);
				}
				else{
					if(opts.onError) opts.onError(body, res, err);
				}
			}
		};
		self.getRequest(options);
	};

	// opts = {onSuccess, onError}
	self.findPersonnelGroupPairs = function(opts){
		var options = {
			accept: "application/json",
			pageSize: 15000,
			path: "/victorWebService/api/Objects/GetAllWithCriteria?&TypeFullName=SoftwareHouse.CrossFire.Common.Objects.GroupMember&DisplayProperties%5B%5D=&WhereClause=GroupType%3D%27SoftwareHouse.NextGen.Common.SecurityObjects.Personnel%27&Arguments%5B%5D="
		}
		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							guid: i.GUID,
							personnel_id: i.TargetObjectID,
							group_id: i.GroupID
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(isEmptyResult(body)){
					opts.onSuccess([], res);
				}
				else{
					if(opts.onError) opts.onError(body, res, err);
				}
			}
		};
		self.getRequest(options);
	};

	// opts = {personnelGuid / personnelID / escortOption, onSuccess, onError}
	self.findPersonnels = function(opts){
		var formData = "";
		formData += "TypeFullName=" + "SoftwareHouse.NextGen.Common.SecurityObjects.Personnel";
		if(opts.personnelID){
			formData += "&WhereClause=" + "ObjectID="+opts.personnelID+"";
		}
		else if(opts.personnelGuid){
			formData += "&WhereClause=" + "GUID='"+opts.personnelGuid+"'";
		}
		else if(opts.escortOption){
			formData += "&WhereClause=" + "EscortOption='"+opts.escortOption+"'";
		}
		else{

		}

		var formDataEndoced = encodeURI(formData);

		var options = {
			path: "/victorWebService/api/Objects/GetAllWithCriteria?"+formDataEndoced,
			accept: "application/json",
			pageSize: 14000
			// body: formDataEndoced
		}

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							guid: i.GUID,
							first_name: i.FirstName,
							middle_name: i.MiddleName,
							last_name: i.LastName,
							name: i.Name,
							email: i.EmailAddress,
							escort_option: i.EscortOption
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(isEmptyResult(body)){
					opts.onSuccess([], res);
				}
				else{
					if(opts.onError) opts.onError(body, res, err);
				}
			}
		};
		self.getRequest(options);
	};

	// opts = {cardNumber}
	self.getTempBadge = function(opts){
		opts.cardNumber = parseInt(opts.cardNumber);
		log.debug("search stored original credentials", opts);
		var key = "card_" + opts.cardNumber;
		var tempCard = db.getItemSync(key);
		log.debug("found stored credentials", tempCard);
		if(tempCard){
			return tempCard;
		}
		else{
			return null;
		}
	}

	// opts = {cardNumber}
	self.deleteTempBadge = function(opts){
		opts.cardNumber = parseInt(opts.cardNumber);
		log.debug("Remove original creditials for", opts);
		var key = "card_" + opts.cardNumber;
		db.removeItemSync(key);
	}

	// opts = {cardNumber, originalCredentialIDs}
	self.setTempBadge = function(opts){
		opts.cardNumber = parseInt(opts.cardNumber);
		var key = "card_" + opts.cardNumber;
		var obj = {originalCredentialIDs: opts.originalCredentialIDs};
		log.debug("Record original credentials for", opts);
		db.setItemSync(key, obj);
	}

	// opts = {credentialID / cardNumber, disabled, onSuccess, onError}
	self.toggleCredential = function(opts){
		log.debug("toggle credential", opts);
		self.findCredentials({
			cardNumber: opts.cardNumber,
			credentialID: opts.credentialID,
			onSuccess: function(credentials){
				var credential = credentials[0];
				if(credential){
					self.updateObject({
						objectType: 'Credential',
						objectID: credential.id,
						attr: 'Disabled',
						val: opts.disabled,
						onSuccess: opts.onSuccess,
						onError: opts.onError
					});
				}
				else{
					if(opts.onError){
						opts.onError(null);
					}
				}
			}
		});
	}

	// opts = {cardNumber}
	self.deleteCredential = function(opts){
		log.debug("delete credential", opts)
		//if(_.isEmpty(opts.cardNumber)){
		//  if(opts.onError){
		//    opts.onError(null);
		//  }
		//  return;
		//}
		self.findCredentials({
			cardNumber: opts.cardNumber,
			onSuccess: function(credentials){
				var credential = credentials[0];
				if(credential){
					self.deleteObject({
						objectType: 'Credential',
						objectID: credential.id,
						onSuccess: opts.onSuccess,
						onError: opts.onError
					});
				}
				else{
					if(opts.onError){
						opts.onError(null);
					}
				}
			},
			onError: opts.onError
		});
	}

	// opts = {cardNumber, onSuccess, onError}
	self.removeCard = function(opts){
		var cb = function(){
			//setTimeout(function(){
				self.deleteTempBadge({cardNumber: opts.cardNumber});
				self.deleteCredential({cardNumber: opts.cardNumber});
				opts.onSuccess();
			//},1000);
		};
		var tempCard = self.getTempBadge({cardNumber: opts.cardNumber});
		if(tempCard){
			// enable original cards, remove temp card
			var originalCredentialIDs = tempCard.originalCredentialIDs;
			if(!_.isEmpty(originalCredentialIDs)){
				var called = 0;
				var callback = function(){
					if (++called == originalCredentialIDs.length) {
						log.debug("Callback called "+called+" times and we're done.");
						cb();
					} else {
						log.debug("Callback called "+called+" times.");
					}
				};
				_.each(originalCredentialIDs, function(credentialID){      
					self.toggleCredential({
						credentialID: credentialID,
						disabled: false,
						onSuccess: callback,
						onError: callback
					});
				});
			}
			
		}
		else{
			// remove card
			self.deleteCredential({
				cardNumber: opts.cardNumber,
				onSuccess: cb,
				onError: cb
			});
		}
	}

	// opts = {credentialID / cardNumber / personnelID}
	self.findCredentials = function(opts){
		//log.debug("search credentials for", opts);
		var formData = "";
		formData += "TypeFullName=" + "Credential";
		if(opts.credentialID){
			formData += "&WhereClause=" + "ObjectID="+opts.credentialID+"";
		}
		else if(opts.cardNumber){
			formData += "&WhereClause=" + "CardNumber="+opts.cardNumber+"";
		}
		else if(opts.personnelID){
			formData += "&WhereClause=" + "PersonnelID='"+opts.personnelID+"'";
		}
		else{

		}

		var formDataEndoced = encodeURI(formData);

		var options = {
			path: "/victorWebService/api/Objects/GetAllWithCriteria?"+formDataEndoced,
			accept: "application/json",
			pageSize: 15000
			// body: formDataEndoced
		}

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							guid: i.GUID,
							personnelID: i.PersonnelID,
							disabled: i.Disabled,
							disabledByInactivity: i.DisabledByInactivity,
							lost: i.Lost,
							stolen: i.Stolen,
							cardNumber: i.CardNumber,
							activationDateTime: i.ActivationDateTime,
							expirationDateTime: i.ExpirationDateTime
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(isEmptyResult(body)){
					opts.onSuccess([], res);
				}
				else{
					if(opts.onError) opts.onError(body, res, err);
				}
			}
		};
		self.getRequest(options);
	};

	// opts = {cardNumber, onSuccess, onError}
	self.checkout = function(opts){
		// find credential
		log.debug("checkout card", opts);
		self.removeCard({
			cardNumber: opts.cardNumber, 
			onSuccess: opts.onSuccess,
			onError: opts.onError
		});
	};

	// opts = {firstName, lastName, activeDate, expiryDate, cardNumber, clearanceIDs, hostPersonnelGuid}
	self.checkin = function(opts){
		var visitorObj = _.clone(opts);
		self.checkout({
			cardNumber: visitorObj.cardNumber,
			onSuccess: function(){
				log.debug("checkin card", opts);
				// temp badge
				if(!_.isEmpty(visitorObj.hostPersonnelGuid)){
					self.findPersonnels({
						personnelGuid: visitorObj.hostPersonnelGuid,
						onSuccess: function(personnels){
							var personnel = personnels[0];
							if(personnel){
								self.findCredentials({
									personnelID: personnel.id,
									onSuccess: function(credentials){
										if(!_.isEmpty(credentials)){
											var storedOriginalCreditials = self.getTempBadge({cardNumber: opts.cardNumber});
											var originalCredentialIDs = [];
											_.each(credentials, function(credential){
												if(credential.disabled != true && credential.stolen != true && credential.lost != true){
													if(storedOriginalCreditials && storedOriginalCreditials.originalCredentialIDs){
														var found = _.find(storedOriginalCreditials.originalCredentialIDs, function(i){
															return credential.id == i;
														});
														if(!_.isEmpty(found)){
															originalCredentialIDs.push(credential.id);
														}
													}
													else{
														originalCredentialIDs.push(credential.id);
													}
												}
											});

											// disable them
											_.each(originalCredentialIDs, function(credentialID){
												self.toggleCredential({
													credentialID: credentialID,
													disabled: true
												})
											});

											// record them
											self.setTempBadge({
												cardNumber: opts.cardNumber,
												originalCredentialIDs: originalCredentialIDs
											})
										}
									}
								});

								self.addCredentialToPersonnel({
									personnelID: personnel.id,
									cardNumber: opts.cardNumber,
									activeDate: opts.activeDate,
									expiryDate: opts.expiryDate
								});
							}
						}
					});
				}
				// regular visitor
				else{
					self.createVisitor(visitorObj);
				}
			}
		});
	};

	// opts = {personnelID, onSuccess, onError}
	self.findPersonnelClearancePairs = function(opts){
		var formData = "";
		formData += "TypeFullName=" + "SoftwareHouse.NextGen.Common.SecurityObjects.PersonnelClearancePair";
		if(opts.personnelID){
			formData += "&WhereClause=" + "PersonnelID="+opts.personnelID+"";
		}

		var formDataEndoced = encodeURI(formData);

		var options = {
			path: "/victorWebService/api/Objects/GetAllWithCriteria?"+formDataEndoced,
			accept: "application/json",
			pageSize: 14000
			// body: formDataEndoced
		}

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							personnelID: i.PersonnelID,
							clearanceID: i.ClearanceID
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(isEmptyResult(body)){
					opts.onSuccess([], res);
				}
				else{
					if(opts.onError) opts.onError(body, res, err);
				}
			}
		};
		self.getRequest(options);
	};

	// opts = {onSuccess, onError}
	self.fetchDirectory = function(opts){
		log.debug("fetchDirectory: call findPersonnelGroups")
		self.findPersonnelGroups({
			onSuccess: function(groups){
				log.debug("fetchDirectory: findPersonnelGroups success, call findPersonnels (1)")
				self.findPersonnels({
					escortOption: self.consts.ESCORTOPTION.ESCORT,
					onSuccess: function(personnels1){
						log.debug("fetchDirectory: findPersonnels (1) returned. call again (2)")
						self.findPersonnels({
							escortOption: self.consts.ESCORTOPTION.NONE,
							onSuccess: function(personnels2){
								log.debug("fetchDirectory: findPersonnels (2) returned. call findPersonnelGroupPairs")
								self.findPersonnelGroupPairs({
									onSuccess: function(pairs){
										log.debug("fetchDirectory: findPersonnelGroupPairs returned.  Processing...")
										// var personnels = personnels1.concat(personnels2);
										var personnels = personnels1;
										_.each(personnels2, function(pers){
											var found = _.find(personnels, function(i){return i.id == pers.id});
											if(!found){
												personnels.push(pers);
											}
										});
										_.each(pairs, function(p){
											var personnel = _.find(personnels, function(i){
												return (i.id == p.personnel_id);
											});
											var group = _.find(groups, function(i){
												return (i.id == p.group_id);
											});
											if(personnel){
												p.personnel_guid = personnel.guid;
											}
											if(group){
												p.group_guid = group.guid;
											}
										})
										opts.onSuccess({
											personnels: personnels,
											groups: groups,
											pairs: pairs
										})
									}
								})
							}
						})
					}
				})
			}
		})
	}

	// opts = {groupID, onSuccess, onError}
	self.findClearancesForGroup = function(opts){
		self.findClearanceGroupPairs({
			groupID: opts.groupID,
			onSuccess: function(pairs){
				var clearanceIDs = _.map(pairs, function(x){return x.clearanceID});
				if(!_.isEmpty(clearanceIDs)){
					self.findAllClearances({
						onSuccess: function(clearances){
							var r = [];
							_.each(clearanceIDs, function(cid){
								var found = _.find(clearances, function(c){ return (c.id == cid) });
								r.push(found);
							});
							opts.onSuccess(r);
						}
					})
					
				}
			}
		});
	}

	// opts = {onSuccess, onError}
	self.findAllClearances = function(opts){
		var options = {
			path: "/victorWebService/api/Objects/GetAll?type=Clearance"
		};
		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							guid: i.GUID,
							name: i.Name
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(isEmptyResult(body)){
					opts.onSuccess([], res);
				}
				else{
					if(opts.onError) opts.onError(body, res, err);
				}
			}
		};
		self.getRequest(options);
	};

	// opts = {groupID(optional), onSuccess, onError}
	self.findClearanceGroupPairs = function(opts){
		var formData = "";
		formData += "TypeFullName=" + "SoftwareHouse.CrossFire.Common.Objects.GroupMember";
		if(opts.groupID){
			formData += "&WhereClause=" + "GroupType='SoftwareHouse.NextGen.Common.SecurityObjects.Clearance' and GroupID='"+opts.groupID+"'";
		}
		else{
			formData += "&WhereClause=" + "GroupType='SoftwareHouse.NextGen.Common.SecurityObjects.Clearance'";
		}

		 var formDataEndoced = encodeURI(formData);

		var options = {
			path: "/victorWebService/api/Objects/GetAllWithCriteria?"+formDataEndoced,
			accept: "application/json",
			pageSize: 12000
			// body: formDataEndoced
		}

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							groupID: i.GroupID,
							clearanceID: i.TargetObjectID
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(opts.onError) opts.onError(body, res, err);
			}
		};
		self.getRequest(options);
	}

	// opts = {guids(optional)/name, onSuccess, onError}
	self.findClearanceGroups = function(opts){
		var formData = "";
		formData += "TypeFullName=" + "SoftwareHouse.CrossFire.Common.Objects.Group";
		formData += "&WhereClause=" + "GroupType='SoftwareHouse.NextGen.Common.SecurityObjects.Clearance' ";
		if(opts.guids){
			formData += "and (";
			for(var i = 0; i<opts.guids.length; i++){
				var g = opts.guids[i];
				formData += " GUID = '"+g+"' ";
				if(i < opts.guids.length-1){
					formData += " or ";
				}
			}
			formData += ")";
		}
		else if(opts.name){
			formData += "and Name='"+opts.name+"'";
		}
		else{
			
		}
		var formDataEndoced = encodeURI(formData);

		var options = {
			path: "/victorWebService/api/Objects/GetAllWithCriteria?"+formDataEndoced,
			accept: "application/json",
			pageSize: 10000
			// body: formDataEndoced
		}

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							guid: i.GUID,
							name: i.Name
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(opts.onError) opts.onError(body, res, err);
			}
		};
		self.getRequest(options);
	};

	// opts = {fromDate, toDate, messageType, onSuccess, onError}
	self.findCardReads = function(opts){
		var form = {
			MessageType: "SoftwareHouse.NextGen.Common.LogMessageFormats."+opts.messageType,
			fromDate: opts.fromDate,
			toDate: opts.toDate,
			PageSize: 1000,
			PageNumber: 1,
			SearchType: "Journal"
		};
		var formqs = querystring.stringify(form);

		var options = {
			path: "/victorWebService/api/Journal/LoadHistoricalMessagesBasic?",
			body: formqs,
			headers: {
				'Accept': 'application/json'
			}
		};

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				var json = JSON.parse(body);
				var r = [];
				if(json == "No Results Found"){
					r = [];
				}
				else{
					// log.debug("read detected");
					var data = json[0];
					_.each(data, function(i){
						if(i.MessageType == 'CardAdmitted' || i.MessageType=='CardRejected'){
							var card = parseXML(i.XmlMessage, "<Card>", "</Card>");
							if(_.isEmpty(card)){
								card = parseXML(i.XmlMessage, "<CHUID>", "</CHUID>");
							}
							var cardNumber = parseInt(card);
							if(cardNumber != NaN){
								card = cardNumber;
							}
							var c = {
								guid: i.GUID,
								message_type: i.MessageType,
								utc: i.MessageUTC,
								card: card,
								location_guid: i.SecondaryObjectIdentity,
								location_name: i.SecondaryObjectName
							}
							r.push(c);
						}
					});
				}
				if(opts.onSuccess) opts.onSuccess(r, res);
			}
			else{
				log.error("read card err:", err);
				if(opts.onError) opts.onError(body, res, err);
			}
		};

		var req = self.postRequest(options);
	};

	// opts = {fromDate, toDate, onSuccess, onError}
	self.findAllCardReads = function(opts){
		self.findCardReads({
			fromDate: opts.fromDate,
			toDate: opts.toDate,
			messageType: "CardAdmitted",
			onSuccess: function(admitted){
				self.findCardReads({
					fromDate: opts.fromDate,
					toDate: opts.toDate,
					messageType: "CardRejected",
					onSuccess: function(rejected){
						var r = admitted.concat(rejected);
						var uniqr = [];
						_.each(r, function(i){
							var found = _.find(uniqr, function(ui){return ui.guid == i.guid;});
							if(!found){
								uniqr.push(i);
							}
						});

						opts.onSuccess(uniqr);
					}
				});
			}
		});
	}

	// opts = {onSuccess, onError}
	self.findDoors = function(opts){
		var options = {
			accept: "application/json",
			pageSize: 10000,
			path: "/victorWebService/api/Objects/GetAllWithCriteria?&TypeFullName=SoftwareHouse.NextGen.Common.SecurityObjects.iStarDoor&DisplayProperties%5B%5D=&Arguments%5B%5D="
		}
		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					var json = JSON.parse(body);
					var r = _.map(json, function(i){
						return {
							id: i.ObjectID,
							guid: i.GUID,
							name: i.Name
						}
					});
					opts.onSuccess(r, res);
				}
			}
			else{
				if(isEmptyResult(body)){
					opts.onSuccess([], res);
				}
				else{
					if(opts.onError) opts.onError(body, res, err);
				}
			}
		};
		self.getRequest(options);
	};

	// opts = {firstName, lastName, cardNumber, activeDate, expiryDate, clearanceGroupGuids / clearanceIDs}
	self.createVisitor = function(opts){
		var formData = "";
		formData += "Type=" + "SoftwareHouse.NextGen.Common.SecurityObjects.Personnel";
		if(self.hasDefaultPartition()){
			formData += "&PropertyNames[]=" + "PartitionID";
		}
		formData += "&PropertyNames[]=" + "FirstName";
		formData += "&PropertyNames[]=" + "LastName";
		formData += "&PropertyNames[]=" + "EscortOption";
		if(self.hasDefaultPartition()){
			formData += "&Propertyvalues[]=" + self.defaultPartition.id;
		}
		formData += "&Propertyvalues[]=" + (opts.firstName || 'None');
		formData += "&Propertyvalues[]=" + (opts.lastName || 'None');
		formData += "&Propertyvalues[]=" + self.consts.ESCORTOPTION.UNESCORTED_VISITOR;
		formData += "&Children[0][Type]=" + "SoftwareHouse.NextGen.Common.SecurityObjects.Credential";
		// formData += "&Children[0][PropertyNames][]=" + "SmartID";
		formData += "&Children[0][PropertyNames][]=" + "CHUID";
		formData += "&Children[0][PropertyNames][]=" + "CardNumber";
		formData += "&Children[0][PropertyNames][]=" + "FacilityCode";
		formData += "&Children[0][PropertyNames][]=" + "ActivationDateTime";
		formData += "&Children[0][PropertyNames][]=" + "ExpirationDateTime";
		// formData += "&Children[0][Propertyvalues][]=" + "";
		formData += "&Children[0][Propertyvalues][]=" + opts.cardNumber;
		formData += "&Children[0][Propertyvalues][]=" + opts.cardNumber;
		formData += "&Children[0][Propertyvalues][]=" + 0;
		formData += "&Children[0][Propertyvalues][]=" + opts.activeDate;
		formData += "&Children[0][Propertyvalues][]=" + opts.expiryDate;
		var formDataEndoced = encodeURI(formData);

		var data = formDataEndoced;
		var options = {
			host: config.host,
			port: 80,
			path: '/victorWebService/api/Objects/Persist?token='+self.authToken,
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'session-id': self.sessionId
			}
		};

		log.debug("create visitor", opts);
		var req = http.request(options, function(res) {
				res.setEncoding('utf8');
				var fullResp = "";
				res.on('data', function (chunk) {
					fullResp += chunk;
				});
				res.on('end', function(){
					var body = fullResp;
					try {
						var json = JSON.parse(body);
						log.debug("body: " + json);
					}
					catch(err) {
						log.error("error:" + err);
						log.error("opts:" + JSON.stringify(options));
						log.error("body:" + body);
						log.error("status code:" + res.statusCode);
					}

					if(isGoodStatusCode(res.statusCode)){
						if(opts.onSuccess){
							var visitorID = null;
							try{
								visitorID = parseXML(body, "'", "'");
							}catch(e){

							}
							if(!visitorID) return;

							if(!_.isEmpty(opts.clearanceIDs)){
								_.each(opts.clearanceIDs, function(clearanceID){
									self.addClearanceToPersonnel({
										clearanceID: clearanceID,
										personnelID: visitorID,
										onSuccess: function(body){
											log.debug(body);
										},
										onError: function(err){
											log.error("err", err);
										}
									})
								});
							}
							else{

							}

							opts.onSuccess(body, res);

						} 
					}
					else{
						if(opts.onError) opts.onError(body, res, null);
					}
				});
		});

		req.write(data);
		req.end();
	}

	// opts = {clearanceID, personnelID}
	self.addClearanceToPersonnel = function(opts){
		log.debug("Adding clearance ", opts.clearanceID, " to personnel ", opts.personnelID);
		var formData = "";
		formData += "Type=" + "SoftwareHouse.NextGen.Common.SecurityObjects.Personnel";
		formData += "&ID=" + opts.personnelID;
		formData += "&Children[0][Type]=" + "SoftwareHouse.NextGen.Common.SecurityObjects.PersonnelClearancePair";
		formData += "&Children[0][PropertyNames]=" + "PersonnelID";
		formData += "&Children[0][PropertyNames]=" + "ClearanceID";
		formData += "&Children[0][Propertyvalues]=" + opts.personnelID;
		formData += "&Children[0][Propertyvalues]=" + opts.clearanceID;
		var formDataEndoced = encodeURI(formData);

		var options = {
			path: "/victorWebService/api/Objects/PersistToContainer?",
			body: formDataEndoced
		}

		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					// var json = JSON.parse(body);
					opts.onSuccess(body, res);
				}
			}
			else{
				if(opts.onError) opts.onError(body, res, err);
			}
		};

		self.postRequest(options);
	}

	// opts = { personnelID, cardNumber, activeDate, expiryDate }
	self.addCredentialToPersonnel = function(opts){
		log.debug("Add credential to personnel", opts);
		var formData = "";
		formData += "Type=" + "Personnel";
		formData += "&ID=" + opts.personnelID;
		formData += "&Children[0][Type]=" + "Credential";
		formData += "&Children[0][PropertyNames]=" + "CardNumber";
		formData += "&Children[0][PropertyNames]=" + "CHUID";
		formData += "&Children[0][PropertyNames]=" + "FacilityCode";
		formData += "&Children[0][PropertyNames]=" + "ActivationDateTime";
		formData += "&Children[0][PropertyNames]=" + "ExpirationDateTime";
		formData += "&Children[0][Propertyvalues]=" + opts.cardNumber;
		formData += "&Children[0][Propertyvalues]=" + opts.cardNumber;
		formData += "&Children[0][Propertyvalues]=" + 0;
		formData += "&Children[0][Propertyvalues]=" + opts.activeDate;
		formData += "&Children[0][Propertyvalues]=" + opts.expiryDate;
		var formDataEndoced = encodeURI(formData);

		var data = formDataEndoced;
		var options = {
			host: config.host,
			port: 80,
			path: '/victorWebService/api/Objects/PersistToContainer?token='+self.authToken,
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'session-id': self.sessionId
			}
		};

		var req = http.request(options, function(res) {
			res.setEncoding('utf8');
			var fullResp = "";
			res.on('data', function (chunk) {
				fullResp += chunk;
			});
			res.on('end', function(){
				var body = fullResp;
				var json = JSON.parse(body);
				//log.debug("body: " + body);
				if(isGoodStatusCode(res.statusCode)){
					if(opts.onSuccess){
						opts.onSuccess(body, res);
					} 
				}
				else{
					if(opts.onError) opts.onError(body, res, null);
				}
			});
		});

		req.write(data);
		req.end();
	}

	// opts = {objectType, objectID, attr, val}
	self.updateObject = function(opts){
		var formData = "";
		formData += "PropertyNames[]=" + opts.attr;
		formData += "&Propertyvalues[]=" + opts.val;
		var formDataEndoced = encodeURI(formData);
		var data = formDataEndoced;

		var options = {
				host: config.host,
				port: 80,
				path: "/victorWebService/api/Objects/Put/"+opts.objectType+"/"+opts.objectID+"?"+"token="+self.authToken,
				method: 'PUT',
				headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'session-id': self.sessionId
				}
		};

		var req = http.request(options, function(res) {
				res.setEncoding('utf8');
				var fullResp = "";
				res.on('data', function (chunk) {
					fullResp += chunk;
				});
				res.on('end', function(){
					var body = fullResp;
					// var json = JSON.parse(body);
					// log.debug("body: " + json);
					if(isGoodStatusCode(res.statusCode)){
						if(opts.onSuccess) opts.onSuccess(body, res);
					}
					else{
						if(opts.onError) opts.onError(body, res, null);
					}
				});
		});

		req.write(data);
		req.end();
	}

	// opts = {objectType, objectID}
	self.deleteObject = function(opts){
		var options = {
			path: "/victorWebService/api/Objects/Delete/"+opts.objectType+"/"+opts.objectID+"?",
			noPatination: true
		}
		options.callback = function(err, res, body){
			if(isGoodStatusCode(res.statusCode)){
				if(opts.onSuccess){
					// var json = JSON.parse(body);
					opts.onSuccess(body, res);
				}
			}
			else{
				if(opts.onError) opts.onError(body, res, err);
			}
		};

		self.deleteRequest(options);
	}

	var isGoodStatusCode = function(statusCode){
		if(statusCode >= 200 && statusCode < 400){
			return true;
		}else{
			return false;
		}
	}

	var isEmptyResult = function(data){
		if(data){
			var json = JSON.parse(data);
			if(json && json.Message == 'No data found for the request'){
			 return true;
			}
		}
		return false;
	}

	var parseXML = function(strToParse, strStart, strFinish){
		var str = strToParse.match(strStart + "(.*?)" + strFinish);
		if (str != null) {
			return str[1];
		}
		else{
			return null;
		}
	}

};


module.exports = CCURE;