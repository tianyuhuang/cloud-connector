var storage = require('node-persist');

storage.init();

module.exports = storage;