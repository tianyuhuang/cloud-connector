const nconf = require('../../../lib/config.js')
const log = require('../../../lib/logger.js')
const AMQPClient = require('../../../lib/sv3/AMQPClient.js')
const Cardholder = require('../../../lib/genetec/entity/Cardholder')
const CardholderGroup = require('../../../lib/genetec/entity/CardholderGroup')
const Visitor = require('../../../lib/genetec/entity/Visitor.js')
const LPRMonitor = require('../../../lib/genetec/lib/LPRMonitor')



// list card holders
if(false){
  Cardholder.getList().then((r) => {
    log.trace({data:r}, "fetchDirectory: returned (called success handler). Pushing to sv3Client")

    resJson = {
      personnels: [],
      groups: [],
      pairs: []
    }

    for(var i in r){
      let e = r[i]
      resJson.personnels.push({
        guid: e.guid,
        first_name: e.firstName,
        last_name: e.lastName,
        email: e.email,
        company_name: e.companyName,
        floor: e.floor
      })
    }

    console.log(resJson)
  })
}
  

// list visitor groups
if(false){
  CardholderGroup.findAllGroups({isVisitorGroup:false, includeMembers: true}).then((r)=>{
    console.log("[Groups] ", r)
  })
}


// create visitor
if(false){
  async function PlayCreateVisitor(){
    let groups = await CardholderGroup.findAllGroups({isVisitorGroup:true})
    let groups_data = groups.map((g)=>{
      return {
        id: g.Guid[0],
        guid: g.Guid[0],
        name: g.Name[0]
      }
    })
    
    let clearance_ids = groups_data.map((g)=>{
      return g.guid
    })

    let json = {
      first_name: 'test_f',
      last_name: 'test_l',
      card: 50011,
      active_date: '12/03/2018 01:00:00.00', //string in format "%m/%d/%Y 00:00:00"
      expiry_date: '12/06/2018 20:00:00.00', //string in format "%m/%d/%Y 00:00:00"
      email: 'test@test.com',
      clearance_ids: clearance_ids //list of group ids to add to
    }
    let visitorGroupName = "Visitors"
    Visitor.checkIn(json, visitorGroupName).then(r =>{
      console.log("ck in completed")
      log.info({visitor: json}, "Check-in completed")
    }).catch(err => {
      console.log("err")
      log.error({json, err}, "Error completing create_visitor command!")
    })
  }

  PlayCreateVisitor()
}