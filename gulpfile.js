'use strict';

const os = require('os');
const pkg = require('pkg');
const path = require('path');
const gulp = require('gulp');
const mocha = require('gulp-mocha');
const gutil = require('gulp-util');
const eslint = require('gulp-eslint');
const npmPkg = require('./package.json');
const { spawn } = require('child_process');
const todo = require('gulp-todo');

const ISS_SCRIPT = 'sv3cc-setup.iss';

gulp.task('default', ['test', 'lint', 'todo'], () => {
	//Do what exactly?
	//install modules
	//run tests
	//lint
	//pkg
});

gulp.task('test', () => {
	return gulp.src(['lib/**/*.spec.js'], {
			read: false
		})
		.pipe(mocha({
			reporter: 'spec'
		}))
		.on('error', gutil.log);
});

gulp.task('pkg', () => {
	let platforms = ('win32' == os.platform() && 'x64' == os.arch()) ? 'node8-win-x86' : 'host';
	console.log("Detected platform %s-%s. Building for %s", os.platform(), os.arch(), platforms);
	
	return pkg.exec([
		'package.json', 
		'--target', platforms,
		'--output', path.resolve(process.cwd(), 'build', npmPkg.name),
		//'--output', path.resolve(__dirname, 'build', [npmPkg.name, npmPkg.version].join('-')),
	]);
});

gulp.task('dist', ['pkg'], () => {
	console.log("Running Inno Setup compile.");
	if ('win32' != os.platform()) return Promise.resolve();
	
	return new Promise((resolve, reject) => {
		const inno = spawn('compil32', ['/cc', path.resolve(process.cwd(), ISS_SCRIPT)]);
		let output = "", errput = "";
		inno.stdout.on('data', data => { output += data });
		inno.stderr.on('data', data => { errput += data });

		inno.on('close', code => {
			if (0 !== code) {
				console.log(output);
				console.error(errput);
				reject(code);
			} else {
				resolve(code);
			}
		})
	})
})

gulp.task('lint', () => {
	return gulp.src(['lib/**/*.js'])
		.pipe(eslint())
		.pipe(eslint.format())
		//.pipe(eslint.failAfterError());
});

gulp.task('todo', function(){
	return gulp.src(['lib/**/*.js', '*.js'])
		.pipe(todo())
		.pipe(gulp.dest('./'));
});
