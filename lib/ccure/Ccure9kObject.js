

const privateData = new WeakMap();

class Ccure9kObject {

	constructor(type, properties, obj={}){
		privateData.set(this, {}); //Initialize new empty private property map

		//Assign all the interesting properties to this instance
		//All other properties are ignored.
		for (const p of properties) {
			privateData.get(this)[p] = this[p] = obj[p];
		}
		Object.freeze(this._initial_state); //Assuming all initial properties are flat.
		//TODO: Add support for deep freeze (deep clone object and recursively freeze each property of type object)
	}

	get Type () { return this.constructor.TYPE }
	set Type (s) {} //Ignore

	get ClassType () { return this.Type }
	set ClassType (s) {} //Ignore


	/* Update the properties of this object with those of the argument.
	 * @params {Object} attrs
	 * @returns {Ccure9kObject} Returns this object after modifications.
	 */
	update_attributes(attrs) {
		Object.keys(attrs).forEach(k => {
			this[k] = attrs[k];
		});
		return this;
	}

	changes() {
		//TODO: return an object whose keys are properties that have changed, and the values are [before, after] tuples
		//returns undefined if there are no changes, or should it return {}?
	}

	/** Serialize this object for saving.  Note: does not include children objects.
	 *
	 * @params {Array} attrs - (optional) A list of property names to serialize. 
	 *   If specified, limits properties serialized to ones listed.  Otherwise all 
	 *   properties are included.
	 * @returns {object} An object containing the required parameters 
	 * for Client.objects_persist() or update()
	 */
	serialize(attrs) {
		let params = {
			Type: this.Type,
			PropertyNames: [],
			PropertyValues: []
		}
		let keys = Object.keys(this);
		if (attrs) keys = keys.filter(k => {
			return attrs.indexOf(k) > -1
		});
		keys.forEach((k) => {
			//If property is not undefined, add it
			if (this[k] !== undefined) {
				params.PropertyNames.push(k);
				params.PropertyValues.push(this[k]);
			}
		})
		return params;
	}
}

module.exports = Ccure9kObject;