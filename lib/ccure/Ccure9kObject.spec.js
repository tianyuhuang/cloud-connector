'use strict'

const chai = require('chai');
const expect = chai.expect;
const Ccure9kObject = require('./Ccure9kObject.js');

describe('Ccure9kObject', function(){
	describe('Constructor', function(){
		it('should require TYPE, and PROPERTIES arguments');
		it('should optionally take parameters to populate properties');
	})

	describe('#Type property', function(){
		it('should return the TYPE name used when instantiating');
	});


	describe('#update_attributes(params)', function(){
		it('should return the original object with updated attributes from arguments', function(){
			let o = new Ccure9kObject("TEST", ['Name', 'UUID']);
			let o2 = o.update_attributes({Name: 'Charley', ObjectID: 123});
			expect(o).to.equal(o2);
			expect(o).to.have.property('Name', 'Charley');
			expect(o).to.have.property('UUID', undefined);
			expect(o).to.have.property('ObjectID', 123);
		})
	})

	describe('#serialize()', function(){
		it('should return an object with required fields for Client.object_perist()');
	})
})