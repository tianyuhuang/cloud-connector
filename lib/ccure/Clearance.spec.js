'use strict'

const chai = require('chai');
const expect = chai.expect;
const Clearance = require('./Clearance.js');

describe('Clearance', function(){
	describe('Static properties', function(){
		describe('#TYPE', function(){
			it('should return a string with the full CCURE TypeName for Clearance', function(){
				expect(Clearance.TYPE).to.be.a('string', 'SoftwareHouse.NextGen.Common.SecurityObjects.Clearance');
			});
		});

		describe('#PROPERTIES', function(){
			it('should return an array with all the (useful) property names', function(){
				expect(Clearance.PROPERTIES).to.be.an('array').that.is.not.empty;
				//include.members doesn't behave as expected...target is superset?
				// expect(Clearance.PROPERTIES).to.include.members([
				// 	'CardNumber', 
				// 	'CHUID', 
				// 	'FacilityCode',
				// 	'ActivationDateTime',
				// 	'ExpirationDateTime',
				// 	'Temporary']);
			});

			//TODO: add reference to client
			it('should be a subset of the schema properties');
			//client.schema_get(Personnel.PROPERTIES).then((schema) => {
			//	expect(schema).to.include.members(Personnel.PROPERTIES);
			//});
		})
	});

	describe('Constructor', function(){
		it('should accept no arguments', function(){
			let c = new Clearance();
			expect(Object.keys(c)).to.include.members(['Name', 'ObjectID'])
		});
		it('should accept some key/values');
	});
})