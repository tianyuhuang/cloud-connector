"use strict";

const log = require('../logger.js').child({module:'VictorClient'});
const url = require('url');
const _ = require('lodash');
const fs = require('fs');
const moment = require('moment');
var request = require('request');


const VERSION = require('../../package.json').version;
const CLIENTNAME = 'SV3';
const USER_AGENT = CLIENTNAME + '-CloudConnector/'+VERSION+' ('+process.platform+' '+process.arch+') node.js/'+process.version;

const ESCORT_NONE	= 0;
const ESCORT_UNESCORTED_VISITOR	= 1;
const ESCORT_ESCORTED_VISITOR	= 2;
const ESCORT_ESCORT	= 3;

const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";


var VictorClient = function(params = {}){
	//if (params === undefined) params = {};
	_.defaults(params, {
		username: process.env.CCURE_USERNAME || 'anonymous',
		password: process.env.CCURE_PASSWORD || 'secret',
		protocol: 'http',
		hostname: process.env.CCURE_HOST || 'localhost',
		port:      80,
		pathname: '/victorWebService/api'
	});

	var base_url = url.format({
		protocol: params.protocol,
		hostname: params.host || params.hostname,
		port:     params.port,
		pathname: params.pathname
	});

	// Configure default request defaults
	var jar = request.jar();
	var request_options = {
		method: 'GET',
		jar: jar,
		baseUrl: base_url,
		encoding: 'utf8',
		json: true, // request JSON and parse body
		//forever: true, //use forever-agent (keepalive)
		timeout: 20000, //default timeout to 10s
		headers: {
			'User-Agent': USER_AGENT,
			'Access-Control-Expose-Headers': 'session-id'
		},
		//qsStringifyOptions: {  //Consider adding here as default
		//	arrayFormat: 'brackets'
		//}
	};
	request = request.defaults(request_options); //replace the global scope request

	this._api = undefined;

	//TODO: Make Login return Promise!
	var self = this;
	this.login = function(callback) {
		if (request_options.headers && request_options.headers['session-id']) {
			delete request_options.headers['session-id'];
		}
		if (request_options.qs && request_options.qs['token']) {
			delete request_options.qs['token'];
		}
		request = request.defaults(request_options);

		request({
			uri: "/Authenticate/Login",
			qs: {
				userName: params.username,
				password: params.password,
				clientName: USER_AGENT
			},
			json: true,
			force: true //never queue this bad boy
		}, function(err, res, auth_token){
			if (err) {
				log.error("Login request error!", err);
				callback(new Error(err));
			} else if (200 == res.statusCode) {
				self.last_login = new Date();
				var session_id = res.headers['session-id'];
				request_options.headers['session-id'] = session_id;
				request_options.qs = { token: auth_token };
				self.session_id = session_id;
				self.auth_token = auth_token;
				request = request.defaults(request_options);
				log.info({
					auth_token: auth_token, 
					session_id: session_id
				}, "Authenticated");
				callback(null, auth_token);
			} else {
				log.warn({
					error: err,
					body: auth_token,
					status: res.statusCode,
					res: res
				}, "Login request failed.");

				callback(new Error("Login Error"));
			}
		});
	}

	this.login_fails = 0;
	this.request_count = 0;

	//log.debug({params: params});
	log.info("initiated");
}

VictorClient.prototype.r = function(params, callback){
	let now = this.last_request = new Date();
	let id = this.request_count++;

	log.trace({id: id, uri:params.uri}, "Request started");

	request(params, (err, res, body) => {
		if (res && 401 === res.statusCode) {
			log.warn({id: id, uri:params.uri}, "Invalid Session. Attempting Login...")
			//Login first, then try request again.
			this.login((err, token) => {
				if (err) {
					//throw err;
					log.error({id: id, uri:params.uri}, "Request failed! Login attempt failed! Trying once more in 2 seconds...");
					setTimeout(() => {
						this.r(params, callback);
					}, 2000);
				} else {
					this.r(params, callback);
				}
			})
		} else {
			log.trace({id: id, uri:params.uri}, "Request completed.");
			callback(err, res, body);
		}
	});

}



//TODO: Refactor API into namespaced modules like Twilio node library
/*
const Api = require('./Api');

Object.defineProperty(VictorClient.prototype,
  'api', {
  get: function() {
    this._api = this._api || new Api(this);
    return this._api;
  }
})

Object.defineProperty(Twilio.prototype,
  'generic', {
  get: function() {
    return this.api.generic;
  }
});
*/

/**
 * Get the API version of the remote CCURE system.  Something like "3.00.390.0057"
 *
 * @param {function} callback - Callback function gets passed two args (err, version)
 * @example
 * // Prints the version.
 * client.generic_version((err, ver) => { console.log((err) ? err : ver) });
 */
VictorClient.prototype.generic_version = function(callback) {
	this.r({
		uri: "/Generic/Version",
		json: true
	}, function(err, res, body) {
		callback(err, body);
	});
}

/**
 * Gets all objects from CCURE, optionally scoped by type 
 * and other arbitrary criteria.  This operation is left pretty
 * open-ended in the CCURE Web Service API documentation.  Assume
 * that any object property name can be used in the WhereClause.
 * Please document features and limitations here as they are discovered.
 *
 * TODO: Client will encounter a 404 error when there are no results.  Fix this by returning [] instead.
 * 
 * @param {string} [TypeFullName=Personnel] - Full name of the Object type to be searched
 * @param {string[]} [DisplayProperties] - An array of Object properties to be returned. Objects are returned with all properties if left blank.
 * @param {string} [WhereClause] - A SQL Where clause passed directly to the database.  Also supports variable substitution (e.g. "GUID=?")
 * @param {Object[]} [Arguments] - An array of arguments to be substituted in the WhereClause search criteria.
 * @param {integer} [PageNumber] - Page to return (if paginated)
 * @param {integer} [PageSize] - Number of results to return per page 
 * @param {boolean} [LoadCollection] - "True to load the container for the objects".  TODO: please clarify this.
 * @returns {Promise} A promise for the transaction
 * 
 * @example
 * //Find 3 escorts
 * client.objects_get_all_with_criteria({
 * 	TypeFullName: 'Personnel',
 * 	DisplayProperties: Personnel.PROPERTIES,
 * 	WhereClause: 'EscortOption=?',
 * 	Arguments: [Personnel.ESCORT_OPTION.ESCORT],
 * 	PageSize: 3,
 * 	PageNumber: 1
 * })
 * .then(function(data){
 * 	console.log(JSON.stringify(data, null, 4));
 * }).catch(function(err){
 * 	console.log(err);
 * });
 * @example
 * //Find all escorts whose name starts with Will
 * client.objects_get_all_with_criteria({
 * 	TypeFullName: 'Personnel',
 * 	DisplayProperties: Personnel.PROPERTIES,
 * 	WhereClause: 'EscortOption=? AND FirstName LIKE ?',
 * 	Arguments: [Personnel.ESCORT_OPTION.ESCORT, 'Will%']
 * })
 * .then(function(data){
 * 	console.log(JSON.stringify(data, null, 4));
 * }).catch(function(err){
 * 	console.log(err);
 * });
 * @example
 * //Find all Groups of Personnel
 * client.objects_get_all_with_criteria({
 * 	TypeFullName: 'SoftwareHouse.CrossFire.Common.Objects.Group',
 * 	WhereClause: 'GroupType=?',
 * 	Arguments: [Personnel.TYPE]
 * })
 * .then(function(data){
 * 	console.log(JSON.stringify(data, null, 4));
 * }).catch(function(err){
 * 	console.log(err);
 * });
 */
VictorClient.prototype.objects_get_all_with_criteria = function(params){
	log.trace({args: params}, "Called objects_get_all_with_criteria")
	return new Promise((resolve, reject) => {
		this.r({
			uri: "/Objects/GetAllWithCriteria",
			qs: params
		}, (err, res, body) => {
			if (err) {
				log.error({error:err, res, body}, "objects_get_all_with_criteria failed!");
				reject(err);
			} else if (404 === res.statusCode) {
				resolve([]); // Empty result set
			} else if (res.statusCode != 200) {
				log.error({error:err, res:res, body:body}, "objects_get_all_with_criteria failed!");
				reject(new Error("Non-200 Error Code returned!"));
			} else {
				resolve(body);
			}
		});
	});
}


VictorClient.prototype.objects_get_all = function(type = Personnel.TYPE){
	return new Promise((resolve, reject) => {
		this.r({
			uri: "/Objects/GetAll",
			qs: {
				type: type
			}
		}, (err, res, body) => {
			if (404 === res.statusCode) {
				resolve([]); // Empty result set
			} else if (err || res.statusCode != 200) {
				log.error({err:err, res:res, body:body}, "objects_get_all failed!");
				reject(err);
			} else {
				resolve(body);
			}
		})
	});
}


VictorClient.prototype.objects_get = function(Type, ObjectID){
	return new Promise((resolve, reject) => {
		this.r({
			uri: `/Objects/Get/${Type}/${ObjectID}`
		}, (err, res, body) => {
			if (404 === res.statusCode) {
				resolve(); // Empty result set
			} else if (err || res.statusCode != 200) {
				log.error({err:err, res:res, body:body}, "objects_get failed!");
				reject(err);
			} else {
				resolve(body);
			}
		})
	});
}


/**
 * Retrieve all the CCURE Object Types.  The result of this is also cached in ObjectTypes.js
 *
 * @returns {Object} A dictionary of actual object type full names and their corresponding friendly name
 * @example
 * client.types_object_types()
 * .then(data => {
 * 	console.log(JSON.stringify(JSON.parse(data), null, 4));
 * }).catch(err => console.log(err));
 */
VictorClient.prototype.types_object_types = function(){
	return new Promise((resolve, reject) => {
		this.r({
			uri: "/Types/ObjectTypes"
		}, (err, res, body) => {
			if (err || res.statusCode != 200) {
				log.error({err:err, res:res, body:body}, "types_object_types failed!");
				reject(err);
			} else {
				resolve(body);
			}
		})
	});
}


/**
 * Persist an object
 * 
 * @param {string} Type - Object Type Full Name
 * @param {string[]} PropertyNames - Named properties of the Object to be stored.
 * @param {string[]} PropertyValues - Values for the properties of the Object to be stored.
 * @param {Object[]} Children - Array of child objects to store in the format { Type:"", PropertyNames:[], PropertyValues:[]}
 * @returns a promise with the return message (note: should be ObjectID and Type)
 * @example
 * // Create a visitor (Personnel object of PersonnelType "none"), and a temporary credential
 * client.objects_persist({
 * 	Type: Personnel.TYPE,
 * 	PropertyNames:  ['FirstName', 'LastName', 'EscortOption', 'GUID', 'EmailAddress'],
 * 	PropertyValues: ['Gary', 'Grabapple', Personnel.ESCORT_OPTION.ESCORTED_VISITOR, 
 * 	  uuid(), 'ggrabapple@visitor.com'],
 * 	Children: [
 * 		{
 * 			Type: Credential.TYPE,
 * 			PropertyNames: ['CHUID', 'CardNumber', 'FacilityCode', 'ActivationDateTime', 'ExpirationDateTime','SmartID', 'Temporary'],
 * 			PropertyValues: [cardNumber, cardNumber, 0, '07/17/2017 00:00:00', '07/19/2017 23:59:59', "''", true]
 * 		}
 * 	]
 * }).then(body => {
 * 	console.log(body);
 * }).catch(err => {
 * 	console.log(err);
 * });
 */
VictorClient.prototype.objects_persist = function({Type='Personnel', PropertyNames=[], PropertyValues=[], Children=[]}){
	log.trace({ Type, PropertyNames, PropertyValues, Children }, "objects.persist called");

	return new Promise((resolve, reject) => {
		this.r({
			uri: "/Objects/Persist",
			method: "POST",
			form: {
				Type: Type,
				PropertyNames: PropertyNames,
				PropertyValues: PropertyValues,
				Children: Children
			},
			//qsStringifyOptions: {
			//	arrayFormat: 'brackets'  //NOT NEEDED! default (indexed) works great!
			//}
		}, (err, res, body) => {
			if (err) {
				log.error({params: arguments[0], err:err, res:res}, "objects_persist failed with Error!");
				reject(err);
			} else if (res.statusCode != 200) {
				let err = new Error(`objects_persist failed with ${res.statusCode}: ${res.statusMessage}`)
				log.error({params: arguments[0], err, res, body},
					`objects_persist failed with ${res.statusCode}: ${res.statusMessage}`);
				reject(err);
			} else {
				log.trace({body:body}, "objects_get_all successful")
				
				//Example body: "Persist Object succeeded. The objectID of the parent is: '5062'"

				let m = body.match(/\d+/); //find first number in the string
				if (m && m.length > 0) {
					resolve({
						Type: Type,
						ObjectID: Number(m[0])
					});
				} else {
					//TODO: reject() should return Error, not string
					let err = new Error(`Unable to parse ObjectID from response to object_persist`);
					log.error({params: arguments[0], err, res, body}, 'Unable to parse ObjectID from response to object_persist')
					reject(err);
				}
			}
		})
	});
}

VictorClient.prototype.objects_persist_to_container = function(parent_type, parent_id, children=[]){
	log.trace({ parent_type, parent_id, children }, 'objects.persist_to_container');

	let form = {
		Type: parent_type,
		ID: parent_id,
		Children: children
	}
	return new Promise((resolve, reject) => {
		this.r({
			uri: '/Objects/PersistToContainer',
			method: 'POST', 
			form: form
		}, (err, res, body) => {
			if (err) {
				log.error({form, err:err, res:res}, "objects_persist_to_container failed with Error!");
				reject(err);
			} else if (200 != res.statusCode) {
				let err = new Error(`objects_persist_to_container failed with ${res.statusCode}: ${res.statusMessage}`);
				log.error({form, err, body, res}, 
					`objects_persist_to_container failed with ${res.statusCode}: ${res.statusMessage}`);
				reject(err);
			} else {
				resolve(body);
			}
		});
	})
}

/**
 * Delete an object
 * 
 * @param {string} Type - Object Type Full Name
 * @param {number} ObjectID - ObjectID of the object to be deleted
 * @returns a promise with the return message
 * @example
 * // Delete Personnel 5012
 * client.objects_delete(Personnel.TYPE, 5012).then(body => {
 * 	console.log(body);
 * }).catch(err => {
 * 	console.log(err);
 * });
 */
VictorClient.prototype.objects_delete = function(Type='Personnel', ObjectID){
	log.trace({
	 	Type: Type,
	 	ObjectID: ObjectID
	}, "objects.delete called");
	
	return new Promise((resolve, reject) => {
		this.r({
			uri: `/Objects/Delete/${Type}/${ObjectID}`,
			method: "DELETE"
		}, (err, res, body) => {
			//log.trace({error: err, res:res, body:body}, "Delete call returned."); //Too much

			if (200 === res.statusCode && body.indexOf('successful') >= 0) {
				//Appears successful!
				log.trace({body:body, type:Type, id:ObjectID}, "Delete successful");
				resolve(body); 
			} else {
				log.error({error:err, res:res, body:body}, "objects_delete failed! ${res.statusCode}: ${res.statusMessage}");
				reject(err);
			}
		});
	});
}


/**
 * Get the schema of a given Object Type. The CCURE9000 web service returns
 * an array of property names.  These names are also available as a static
 * reference in each object (e.g. Personnel.PROPERTIES).
 *
 * @param {string} type - Object Type name, full name or short name.
 * @returns a Promise that will return an array of object property names
 * @example
 * //Properties of VisitHostPair
 * client.schema_get('SoftwareHouse.NextGen.Common.SecurityObjects.VisitHostPair')
 * .then(data => console.log(data));
 * // [ 'ObjectID', 'VisitID', 'PersonnelID', 'Notify', 'ClassType' ]
 */
VictorClient.prototype.schema_get = function(type){
	log.trace({ Type: type }, "schema.get() called");

	return new Promise((resolve, reject) => {
		this.r({
			uri: "/Schema/Get",
			qs: {
				type: type
			}
		}, (err, res, body) => {
			if (err) {
				log.error({type:type, error:err, res:res, body:body}, "Schema.get() error!");
				reject(err);
			} else if (res.statusCode != 200) {
				log.warn({type:type, res:res, body:body}, "Schema.get() failed!")
				//TODO: reject() should return Error, not string
				reject(body);
			} else {
				//log.trace({body:body}, 'schema.get() returned successful.')
				resolve(body);
			}
		})
	});
}


/**
 * Get Journal Types
 *
 * Pulls a list of Journal Types from CCURE9000.  Note, this request requires 
 * the Authentication Token to be sent as a query parameter named "key" and NOT "token".
 *
 */
VictorClient.prototype.types_journal_types = function() {
	return new Promise((resolve, reject) => {
		this.r({
			uri: '/Types/JournalTypes',
			qs: {
				key: this.auth_token
			}
		}, (err, res, body) => {
			if (err) {
				log.error({type:type, error:err, res:res, body:body}, "types_journal_types() error!");
				reject(new Error(err));
			} else if (res.statusCode != 200) {
				log.warn({args:arguments, res:res, body:body}, "types_journal_types() failed!")
				reject(body); //TODO: reject with an Error instead of String
			} else {
				log.trace({body:body}, 'types_journal_types() returned successful.')
				resolve(body);
			}
		});
	});
}



/**
 * Get Journal events
 *
 * Note that successful return is an array with another array of Journal objects.
 * This is flattened before being returned.
 */
VictorClient.prototype.journal_load_historical_messages_basic = function({
	MessageType='SoftwareHouse.NextGen.Common.LogMessageFormats.CardAdmitted', 
	FromDate=moment().subtract(5, 's'),
	ToDate=moment(),
	PageSize=1000,
	PageNumber=1,
	SearchType='Journal'
}) {
	log.trace("journal_load_historical_messages_basic()");

	return new Promise((resolve, reject) =>{
		this.r({
			uri: "/Journal/LoadHistoricalMessagesBasic",
			method: 'POST',
			form: {
				MessageType, 
				FromDate: FromDate.format(FORMAT_DATETIME),
				ToDate: ToDate.format(FORMAT_DATETIME),
				PageSize,
				PageNumber,
				SearchType
			}}, (err, res, body) => {
				if (err) {
					log.error({type:type, error:err, res:res, body:body}, "journal_load_historical_messages_basic() error!");
					reject(err);
				} else if (res.statusCode != 200) {
					log.warn({args:arguments, res:res, body:body}, "journal_load_historical_messages_basic() failed!")
					reject(body); //TODO: reject with an Error instead of String
				} else {
					log.trace({body:body}, 'journal_load_historical_messages_basic() returned successful.')
					if (body === 'No Results Found') {
						resolve([]); //Unlike other calls, this one returns 200 status and a string when no results.
					} else {
						resolve(body[0]);
					}
				}
			}
		)
	})
}


module.exports = VictorClient;