'use strict'

require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;
const random = require('../util/random.js');
const moment = require('moment');
const uuid = require('uuid/v4');
const Client = require('./Client.js');

//Note: We are testing Client here, try not to rely on other classes
const Personnel = require('./Personnel');
const PersonnelType = require('./PersonnelType');
const Credential = require('./Credential');

const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";

const trash = [];

const client = new Client();


describe('Client', function(){

	after(done => {
		//destroy all objects in the trash... in parallel!
		Promise.all(trash.map((o, i) => { 
			return client.objects_delete(o.Type, o.ObjectID) 
		}))
		.then(r => {
			done();
		})
		.catch(err => {
			console.log("Error deleting all the objects!", err)
			done();
		});
	})

	describe('#login()', function(){
		it('should return with an auth_token', function(done){
			client.login(function(err, token) {
				if (err) {
					done(err);
				} else {
					expect(token).to.be.a('string');
					expect(token).to.have.lengthOf(36);
					done();
				}
			});
		});
	});

	describe('#generic.version()', function(){
		it('should return the version of the CCURE/Victor Web Service', function(done){
			client.generic_version(function(err, version) {
				expect(version).to.be.a('string');
				expect(version.split('.')).to.have.lengthOf(4);
				done(err);
			});
		});
	});

	//TODO: test for various error returns.  POST /Api/Objects/Persist can return: Error or non-200 status codes.
	describe('#objects.persist()', function(){
		it('should take an object of object properties and values, and return the ID', function(){
			let firstName = random.firstName()
			,	lastName =  random.lastName()
			,	cardNumber = random.cardNumber()
			,	activationTime = moment().startOf('day')
			,	expirationTime = moment().endOf('day').add(7, 'd')
			,	timeFormat = "MM/DD/YYYY HH:mm:ss";

			return client.objects_persist({
				Type: Personnel.TYPE,
				PropertyNames:  ['FirstName', 'LastName', 'EscortOption', 'GUID', 'EmailAddress'],
				PropertyValues: [firstName, lastName, Personnel.ESCORT_OPTION.ESCORTED_VISITOR, 
					uuid(), `${firstName}.${lastName}@visitor.com`],
				Children: [
					{
						Type: Credential.TYPE,
						PropertyNames: ['CHUID', 'CardNumber', 'FacilityCode', 'ActivationDateTime', 'ExpirationDateTime','SmartID', 'Temporary'],
						PropertyValues: [cardNumber, cardNumber, 0, activationTime.format(timeFormat), expirationTime.format(timeFormat), "''", true]
					}
				]
			})
			.then( (result) => {
				expect(result).to.be.an('object');
				expect(result).to.have.property('Type', Personnel.TYPE);
				expect(result).to.have.property('ObjectID').that.is.a('number');
				trash.push(result);
			});
		});
	});

	
	describe('#objects.get_all()', function() {
		this.timeout(10000); //There may be a large number to return, may exceed default 2000ms
		it('should return an array of some object', function(){
			return client.objects_get_all(PersonnelType.TYPE)
			.then(function(data){
				expect(data).to.be.a('Array');

				// Expect there are default Personnel already there
				expect(data).to.have.lengthOf.above(0);
			});
		});
	});

	describe('#objects.get()', function() {
		//before() //Persist an object
		
		//Create an object and then Get it.
		it('should return the specific object requested');

		//Delete it, then try getting it again
		it('should return undefined if the object requested does not exist');
	});


	//TODO: update with use of new random();
	function createTestPersonnel() {
		return new Promise((resolve, reject) => {
			let person = {
				FirstName: random.firstName(),
				LastName: random.lastName(),
				GUID: uuid(),
				get EmailAddress() {
					return `${this.FirstName}.${this.LastName}@visitor.com`
				},
				CardNumber: random.cardNumber(),
				ActivationTime: moment().startOf('day').format(FORMAT_DATETIME),
				ExpirationTime: moment().endOf('day').add(7, 'd').format(FORMAT_DATETIME)
			}
			

			client.objects_persist({
				Type: Personnel.TYPE,
				PropertyNames:  ['FirstName', 'LastName', 'EscortOption', 'GUID', 'EmailAddress'],
				PropertyValues: [person.FirstName, person.LastName, Personnel.ESCORT_OPTION.ESCORTED_VISITOR, 
					person.GUID, person.EmailAddress],
				Children: [
					{
						Type: Credential.TYPE,
						PropertyNames: ['CHUID', 'CardNumber', 'FacilityCode', 'ActivationDateTime', 
							'ExpirationDateTime','SmartID', 'Temporary'],
						PropertyValues: [person.CardNumber, person.CardNumber, 0, person.ActivationTime, 
							person.ExpirationTime, "''", true]
					}
				]
			}).then((result) => {
				resolve(Object.assign(person, result));
			}).catch(reject);
		});
	}

	describe('#createTestPersonnel() [SPEC HELPER]', function(){
		it("should create a valid Personnel record for testing", function(){
			return createTestPersonnel().then(function(person){
				expect(person).to.have.property('FirstName');
				expect(person).to.have.property('LastName');
				expect(person).to.have.property('GUID');
				expect(person).to.have.property('EmailAddress');
				expect(person).to.have.property('CardNumber');
				expect(person).to.have.property('ActivationTime');
				expect(person).to.have.property('ExpirationTime');
				expect(person).to.have.property('Type');
				expect(person).to.have.property('ObjectID');
				trash.push(person);
			})
			.catch((err) => {
				console.log(err);
			})
		});
	});


	describe('#objects.delete()', function(){
		let personnel = null; //Personnel Object to be deleted

		//First let's create a new Personnel with Credential
		before((done) => {
			createTestPersonnel()
			.then((person) => {
				personnel = person;
				done();
			})
			.catch((err) => {
				console.log("Error creating Personnel before testing Delete!", err);
				throw err;
			});
		});


		it('should take an object with Type and ObjectID, and delete the corresponding object', function(){
			return client.objects_delete(Personnel.TYPE, personnel.ObjectID)
			.then((result) => {
				expect(result).to.be.a('string', 'The object is successfully deleted');
			});
				
		});
	});


	describe('#schema.get()', function(){
		it('should take a Type and return the properties of that Object Type', function(){
			return client.schema_get(Credential.TYPE)
			.then((result) => {
				expect(result).to.be.an('array');
				expect(result).to.include.members(Credential.PROPERTIES);
			});
		})
		it('should reject promise if the requested type is invalid.');
		// calls reject with error message "Invalid Type"
	})

	describe('#objects.get_all_with_critieria()', function(){
		it('should take take some parameters...');
	})

	describe('#types_journal_types', function(){
		it('should return an object with properties for each Journal message Type', function(){
			return client.types_journal_types()
			.then((result) => {
				expect(result).to.be.an('object');
				expect(result).to.have.property('SoftwareHouse.NextGen.Common.LogMessageFormats.CardAdmitted');
				expect(result).to.have.property('SoftwareHouse.NextGen.Common.LogMessageFormats.CardRejected');
				expect(result).to.have.property('SoftwareHouse.CrossFire.Common.LogMessageFormats.OperatorLogin');
			});
		});
	});

	describe('#journal_load_historical_messages_basic()', function(){
		it('should return empty array if no events were found', function(){
			return client.journal_load_historical_messages_basic({})
				.then((result, error) => {
					expect(error).to.be.undefined;
					expect(result).to.be.an('array').with.lengthOf(0);
				}
			);
		});

		it('should return an array with some elements', function(){
			return client.journal_load_historical_messages_basic({
				MessageType: 'SoftwareHouse.CrossFire.Common.LogMessageFormats.OperatorLogin',
				FromDate: moment().subtract(3, 'd')
			})
				.then((result, error) => {
					expect(error).to.be.undefined;
					expect(result).to.be.an('array').with.length.greaterThan(0);
				}
			);
		});
	});

});
