'use strict'

require('dotenv').config()
const Client = require('./Client.js');
const Personnel = require('./Personnel.js');
const Credential = require('./Credential.js');
const uuid = require('uuid/v4');
const random = require('../util/random.js');
const fs = require('fs');

const client = new Client();

//Helpers for earlier tests
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
let cardNumber = getRandomInt(50000,999999999);


/* Schema */
//client.schema_get('SoftwareHouse.NextGen.Common.SecurityObjects.VisitHostPair')
//.then(data => console.log(data));

/* Journal Types 
client.login((token) => {
	client.types_journal_types()
	.then((result) => {
		fs.writeFile('JournalTypes.js', JSON.stringify(result, null, 4));
	})
	.catch((err) => {
		console.log("Failed!", err);
	});
}); */

/* Find Personnel by GUID -- SUCCESSFUL -- WORKING!
client.objects_get_all_with_criteria({
	TypeFullName: 'Personnel', 
	WhereClause: `GUID='f62c9b53-4489-442f-997e-008753285ce1'`
	//Arguments: 'f62c9b53-4489-442f-997e-008753285ce1'
}).then(data => console.log(data))
.catch(err => console.log(err));
*/




/* Default Partition 
client.objects_get_all_with_criteria({
	TypeFullName: 'Partition', 
	WhereClause: 'IsDefaultPartition=?',
	Arguments: [true]})
.then(data => console.log(data))
.catch(err => console.log(err));
*/

/* Partitions
client.objects_get_all("Partition")
.then(data => { console.log(data) })
.catch(err => { console.log(err) });
 */

/* Persist Personnel with credential -- WORKING -- SUCCESSFUL
client.objects_persist({
	Type: Personnel.TYPE,
	PropertyNames:  ['FirstName', 'LastName', 'EscortOption', 'GUID', 'EmailAddress'],
	PropertyValues: ['Gary', 'Grabapple', Personnel.ESCORT_OPTION.ESCORTED_VISITOR, 
		uuid(), 'ggrabapple@visitor.com'],
	Children: [
		{
			Type: Credential.TYPE,
			PropertyNames: ['CHUID', 'CardNumber', 'FacilityCode', 'ActivationDateTime', 'ExpirationDateTime','SmartID', 'Temporary'],
			PropertyValues: [cardNumber, cardNumber, 0, '07/17/2017 00:00:00', '07/19/2017 23:59:59', "''", true]
		}
	]
}).then(body => {
	console.log(body);
}).catch(err => {
	console.log(err);
});
*/

/* Persist Personnel 
//Note this create new Personnel of type "None" (should probably assign type with CCURE >= 2.50)
client.objects_persist({
	Type: Personnel.TYPE,
	PropertyNames:  ['FirstName', 'LastName', 'EscortOption', 'GUID', 'EmailAddress'],
	PropertyValues: ['Daffy', 'Duck', Personnel.ESCORT_OPTION.ESCORTED_VISITOR, 
	'd7f5107f-80bb-4471-9ec7-1beac0fd39e6', 'dduck@visitor.com']
}).then(body => {
	console.log("done?");
	console.log(body);
}).catch(err => {
	console.log(err);
});
*/

/* Persist Credential -- SUCCESSFUL
let r = new random();
client.objects_persist({
	Type: 'SoftwareHouse.NextGen.Common.SecurityObjects.Credential',
	PropertyNames:  ['CHUID', 'CardNumber', 'FacilityCode', 'ActivationDateTime', 'ExpirationDateTime', 'SmartID', 'Name', 'PersonnelID', 'AssociationCategory',
  'OrganizationalCategory',
  'OrganizationalIdentifier',
  'PersonnelIdentifier', 'Status'],
	PropertyValues: [r.CardNumber, r.CardNumber, 0, '07/17/2017 00:00:00', '07/28/2017 23:59:59', "''", 'Test 123', 5272, 0, 0, 0, 0, 0]
}).then(body => {
	console.log("done?");
	console.log(body);
}).catch(err => {
	console.log(err);
});
*/

/* Persist Credential -- SUCCESSFUL -- With absolute minimal properties
let r = new random();
client.objects_persist({
	Type: 'SoftwareHouse.NextGen.Common.SecurityObjects.Credential',
	PropertyNames:  ['CHUID', 'CardNumber', 'FacilityCode', 
	'ActivationDateTime', 'ExpirationDateTime', 'SmartID', 'Name', 'PersonnelID', 'Status'],
	PropertyValues: [r.CardNumber, r.CardNumber, 0, '07/17/2017 00:00:00', '07/28/2017 23:59:59', "''", 'Test 321', 5314, 0]
}).then(body => {
	console.log(body);
}).catch(err => {
	console.log(err);
});


/* Schema */
client.schema_get('SoftwareHouse.NextGen.Common.SecurityObjects.PersonnelClearancePair')
.then(data => console.log(data));


/*
//SoftwareHouse.NextGen.Common.SecurityObjects.Credential
client.schema_get('SoftwareHouse.CrossFire.Common.Objects.Partition')
.then(data => console.log(data))
.catch(err => {
	console.log("schema_get returned error!", err)
}); */


/* Group Members
client.objects_get_all_with_criteria({
	TypeFullName: 'SoftwareHouse.CrossFire.Common.Objects.GroupMember',
	WhereClause: 'GroupType=?',
	Arguments: [Personnel.TYPE]
})
.then(function(data){
	console.log(JSON.stringify(data, null, 4));
}).catch(function(err){
	console.log(err);
});
*/

/* Get API version */
//client.generic_version((err, ver) => { console.log((err) ? err : ver) });


/* Get Object Types 
client.types_object_types()
.then(data => {
	console.log(JSON.stringify(data, null, 4));
}).catch(err => console.log(err));
*/

/* Personnel Groups 
client.objects_get_all_with_criteria({
	TypeFullName: 'SoftwareHouse.CrossFire.Common.Objects.Group',
	WhereClause: 'GroupType=?',
	Arguments: [Personnel.TYPE]
})
.then(function(data){
	console.log(JSON.stringify(data, null, 4));
}).catch(function(err){
	console.log(err);
});
*/

/* Partial name match 
client.objects_get_all_with_criteria({
	TypeFullName: 'Personnel',
	DisplayProperties: Personnel.PROPERTIES,
	WhereClause: 'EscortOption=? AND FirstName LIKE ?',
	Arguments: [Personnel.ESCORT_OPTION.ESCORT, 'Will%'],
	PageSize: 5,
	PageNumber: 1
})
.then(function(data){
	console.log(JSON.stringify(data, null, 4));
}).catch(function(err){
	console.log(err);
});
*/

/* Find Escort named Bill 
client.objects_get_all_with_criteria({
	TypeFullName: 'Personnel',
	DisplayProperties: Personnel.PROPERTIES,
	WhereClause: 'EscortOption=? AND FirstName=?',
	Arguments: [Personnel.ESCORT_OPTION.ESCORT, 'Bill'],
	PageSize: 5,
	PageNumber: 1
})
.then(function(data){
	console.log(JSON.stringify(data, null, 4));
}).catch(function(err){
	console.log(err);
});
*/

/* Find All Escorts
client.objects_get_all_with_criteria({
	TypeFullName: 'Personnel',
	DisplayProperties: Personnel.PROPERTIES,
	WhereClause: 'EscortOption=?',
	Arguments: [Personnel.ESCORT_OPTION.ESCORT],
	PageSize: 1,
	PageNumber: 1
})
.then(function(data){
	console.log(JSON.stringify(data, null, 4));
}).catch(function(err){
	console.log(err);
});
*/

/* Find All Personnel 
client.objects_get_all(Personnel.TYPE)
.then(function(data){
	console.log(JSON.stringify(data, null, 4));
}).catch(function(err){
	console.log(err);
});
*/

/* Example: Write pretty JSON to file */
//fs.writeFile('objects_get_all_with_criteria.json', JSON.stringify(obj, null, 4));
