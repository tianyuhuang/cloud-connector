"use strict";

/* Credential Object */

const Ccure9kObject = require('./Ccure9kObject.js');
const moment = require('moment');
const _ = require('lodash');

const TYPE = 'SoftwareHouse.NextGen.Common.SecurityObjects.Credential'
const PROPERTIES = [
	"ObjectID",
	"GUID",
	"LastModifiedByID",
	"LastModifiedTime",
	"Protected",
	"Template",
	"AccessType",
	"Active",
	"Disabled",
	"Lost",
	"Stolen",
	"PersonnelID",
	"CardInt1",
	"CardInt2",
	"CardInt3",
	"CardInt4",
	"HMAC",
	"CHUID",
	"Name",
	"CHUIDFormatID",
	"ActivationDateTime",
	"ExpirationDateTime",
	"AgencyCode",
	"CredentialIssue",
	"CredentialSeries",
	"FacilityCode",
	"SystemCode",
	"IssueCode",
	"CardNumber",
	"PIN",
	"BadgePrintDate",
	"BadgeLayoutId",
	"FingerprintDateTime",
	"PartitionID",
	"SmartCardDateTime",
	"AssociationCategory",
	"OrganizationalCategory",
	"OrganizationalIdentifier",
	"PersonnelIdentifier",
	"DisabledByInactivity",
	"DisabledByInactivityDateTime",
	"ExpectedDate",
	"InactivityExempt",
	"DisabledByID",
	"Status",
	"IssueDate",
	"SmartID",
	"BLEEnabled",
	"AssignRandomCardNumber"
]
const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";


class Credential extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);

		//Fill in Defaults
		_.defaults(this, {
			Name: `SV3 Visitor Badge ${this.CardNumber}`,
			CHUID: this.CardNumber,
			FacilityCode: 0,
			SmartID: "''", //SmartID must be empty string to pass CCURE validation (>= v.2.50)
			Temporary: true,
			ActivationDateTime: moment().startOf('day').format(FORMAT_DATETIME),
			ExpirationDateTime: moment().endOf('day').format(FORMAT_DATETIME),
			Status: 0
		});
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }
	static get FORMAT_DATETIME () { return FORMAT_DATETIME }
}

module.exports = Credential;
