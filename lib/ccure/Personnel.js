"use strict";

/* Personnel Object */

const Ccure9kObject = require('./Ccure9kObject.js');
const _ = require('lodash');
const uuid = require('uuid/v4');

const TYPE = 'SoftwareHouse.NextGen.Common.SecurityObjects.Personnel'
const PROPERTIES = [
	'ObjectID',
	'GUID',
	'PartitionID',
	'PersonnelTypeID',
	'EmailAddress',
	'FirstName',
	'MiddleName',
	'LastName',
	'ProperName',
	'Name',
	'EscortOption',
	'Disabled',
	'CanHost',
	'LostCredentials',
	'StolenCredentials',
//	'Text1',
//	'Text2',
//	'Text3', //Not using these right now.
//	'Text4',
//	'Text5',
	'LastModifiedTime',
	'LastModifiedByID',
	'LastModifiedByTypeName',
	'LastVisitTime',
	'CheckedInVisit',
	'CheckedInVisitID',
//	'PrimaryPortraitFullImage',  //Note: this can be big. Exclude from queries unless needed.
]
const ESCORT_OPTION = {
	NONE: 0,
	UNESCORTED_VISITOR: 1,
	ESCORTED_VISITOR: 2,
	ESCORT: 3
}

//TODO: inherit from Ccure9kObject
class Personnel extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);

		//Fill in Defaults
		_.defaults(this, {
			GUID: uuid()
		});
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }
	static get ESCORT_OPTION () { return ESCORT_OPTION }

	//get Name () { return `${this.FirstName} ${this.LastName}` }

	//TODO: add Cards getter with WeakMap private data
	//TODO: override serialize() method to support Children.  See Visit for example.
}

module.exports = Personnel;
