"use strict";

const log = require('../logger.js').child({module:'RequestQueue'});
const request = require('request');
const _ = require('lodash');

/**
 * RequestQueue is a wrapper round the request module to manage requests
 * to the Victor/CCURE Web Service.  Use this to easily work around their
 * authentication/session management nonsence.
 */
var RequestQueue = function(defaults){
	this.defaults = {
		jar: request.jar(),
		encoding: 'utf8'
	};

	this.merge(defaults);

	this.q = [];
	this.running = true;
}

RequestQueue.prototype.merge = function(params){
	_.merge(this.defaults, params);
	this.r = request.defaults(this.defaults);
}

RequestQueue.prototype.request = function(params, callback, force = false){
	var self = this;
	setTimeout(function(){
		if (force || self.running) {
			self.r(params, callback);
		} else {
			self.queue(params, callback);
		}
	}, 1);
}

RequestQueue.prototype.queue = function(params, callback){
	this.q.push(new DeferredRequest(params, callback));
	log.debug({length: this.q.length}, "Deferred %s", params.uri);
}

RequestQueue.prototype.pause = function(){
	this.running = false;
	log.debug("Queue paused.");
}

RequestQueue.prototype.resume = function(){
	this.running = true;
	log.debug({length: this.q.length}, "Queue resumed.");
	while(this.q.length > 0 && this.running) {
		var next = this.queue.shift();
		this.request(next.params, next.callback);
	}
}


var DeferredRequest = function(params, callback){
	this.params = params;
	this.callback = callback;
}

module.exports = RequestQueue;