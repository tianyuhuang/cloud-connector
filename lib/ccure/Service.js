"use strict";

/** Service
 *  Implements Facade pattern to simplify operations that require multiple calls
 *
*/

const log = require('../logger.js').child({module: 'CcureService'});
const moment = require('moment');
const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');
const Credential = require('./Credential.js');
const Clearance = require('./Clearance.js');
const PersonnelClearancePair = require('./PersonnelClearancePair.js');
const Visit = require('./Visit.js');
const JournalPoller = require('./JournalPoller.js');
const _ = require('lodash');
const config = require('../config.js');
const util = require('./util');
const Criteria = require('./Criteria.js');

const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";





class CcureService {

	constructor(client) {
		this._client = client;
	}

	get client () {
		return this._client;
	}

	login() {
		return new Promise((resolve, reject) => {
			this.client.login(function(err, token) {
				if (err) {
					reject(err);
				} else {
					resolve(token);
				}
			});

		})
	}

/* CRUD Operations: Save (create/update), Find (read), and Destroy (delete) */

	/** Create or Update a Ccure9kObject as appropriate 
	 * (based on whether the object has an ObjectID property)
	 *
	 * @params {Ccure9kObject} obj - The object to save. Must extend Ccure9kObject or implement #serialize()
	 * @returns {Promise} Promise for the action.  Resolve handler passes the saved object back (with ObjectID populated)
	 */
	save(obj) {
		return this.client.objects_persist(obj.serialize())
		.then((result) => {
			return obj.update_attributes(result);
		})
	}

	/**
	 * Assign one or more children to a parent.  This actually creates the Children and
	 * assigns them to the parent only if the child does not already exist, and the child
	 * has a "belongs_to" relationship (one of its properties is a foreign key to the parent).
	 * This method does not support many-to-many relationships, or assiging children that
	 * already exist.
	 *
	 * @param {Ccure9kObject} parent - The parent object to which the Children will be added
	 * @param {Array<Ccure9kObject>} Children - The children to assign.
	 * @returns {Promise}
	 */
	assign(parent, children=[]) {
		return this.client.objects_persist_to_container(
			parent.Type,
			parent.ObjectID, 
			[].concat(children).map(c => {
				return c.serialize(); //Ensure that Children is Array, then serialize them all
			})
		);
	}


	/** Find objects of a given type matching (optional) criteria.
	 * Criteria is a simple exact match using all criteria (concats all 
	 * parameters using AND statement).  This is a basic wrapper around
	 * Client.objects_get_all_with_criteria().  The major difference is
	 * that find() will return an array of instantiated objects of the
	 * expected type.
	 * 
	 * @param {Ccure9kObject SubClass} type - The object type you wish to find
	 * @param {Object} criteria - Property names and corresponding values
	 */
	find(typeObj, criteria=[], limit=1000, page=1) {
		
		//Format criteria Rails style (["WHERE STATEMENT", param1, param2, param3, ...])
		criteria = Criteria.format(criteria);

		let params = {
			TypeFullName: typeObj.TYPE,
			WhereClause: criteria.shift(),
			Arguments: criteria,
			PageSize: limit,
			PageNumber: page
		};
		//Keep this here in case there are more fun exceptions like GUID
		//log.trace(params, "Find is about to objects_get_all_with_criteria")
		return this.client.objects_get_all_with_criteria(params).then(result => {
			log.trace({
				type:typeObj.TYPE, 
				criteria, 
				results: result.map(i => { return _.pick(i, ['ObjectID', 'GUID'])})
			}, '#find returning results');

			return result.map(attrs => { return new typeObj(attrs) });
		});
	}


	/** Wrapper around find() which synchronously calls find() until all 
	  results have been found. 

	  TODO: This is memory heavy. Revise to be more efficient.  Profile to see garbage collection performance
	  TODO: Change to stream interface
	*/
	findAll(typeObj, criteria, page=1, results=[]) {
		const limit = 1000;

		return this.find(typeObj, criteria, limit, page).then(res => {
			if (res.length < limit) {
				return results.concat(res);
			} else {
				page += 1;
				return this.findAll(typeObj, criteria, page, results.concat(res));
			}
		})
	}


	/** Find an object using multiple parallel searches with different criteria and
	 * return the first result based on priority.
	 * 
	 * @param {Ccure9kObject Subclass} type - The object type to find
	 * @param {Object} criteria - An object with name-value pairs used to search
	 * @param {Array[]} priority - An array of arrays, containing key combinations to search for
	 * @param {Object} scope - Additional criteria applied to every query without regard to priority
	 * @returns {Promise<Ccure9kObject>} the first matching object
	 * 
	 * @example
	 * //Search for Personnel using GUID, then EmailAddres, then both names
	 * findFirstMatch(Personnel, {
	 *    GUID:'12345-6789-12346789', 
	 *    EmailAddress:'test@example.com',
	 *    FirstName: 'John',
	 *    LastName: 'Smith'},
	 *    [['GUID'], ['EmailAddress'], ['Firstname', 'LastName']])
	 * .then(person => {
	 *    //output the first person found
	 *    console.log(person)
	 *  })
	 */
	findFirstMatch(type, criteria=[], priority, scope={}) {
		return this.findMultiple(type, criteria, priority, scope)
		.then(results => {
			return [].concat(...results)[0]; //Flatten array and return first element
		})
	}

	//TODO: work on nomenclature for #findFirstMatch() and #findMultiple(); 'priorities' should maybe be order, combinations? Ugh.
	/** Find objects using various combinations of search criteria in parallel and
	 * return the results based in order of priority given.
	 * 
	 * @param {Ccure9kObject Subclass} type - The object type to find
	 * @param {Object} criteria - An object with name-value pairs used to search
	 * @param {Array[]} priority - An array of arrays, containing key combinations to search for
	 * @param {Object} scope - Additional criteria applied to every query without regard to priority
	 * @returns {Array<Array>} All the result sets for all priorities given.
	 * 
	 * @example
	 * //Search for Personnel using GUID, then EmailAddres, then both names
	 * findFirstMatch(
	 *    Personnel,
	 *    {
	 *        GUID:'12345-6789-12346789', 
	 *        EmailAddress:'test@example.com',
	 *        FirstName: 'John',
	 *        LastName: 'Smith'
	 *    },
	 *    [['GUID'], ['EmailAddress'], ['Firstname', 'LastName']])
	 * .then(person => {
	 *    //output the first person found
	 *    console.log(person)
	 *  })
	 */
	findMultiple(type, criteria=[], priority, scope={}) {
		log.trace({type:type.TYPE, criteria, priority}, "#findMultiple()")
		
		return Promise.all(priority.map(i => {
			let combined_criteria = Criteria.and(_.pick(criteria, i), scope);

			return this.find(type, combined_criteria )
		}))
	}


	/** Destroy
	 * Deletes an object from CCURE
	 * @param {Ccure9kObject} obj - The object to destroy
	 * @returns {Promise<Ccure9kObject>} returns the deleted object
	 */
	destroy(obj) {
		return this.client.objects_delete(obj.Type, obj.ObjectID)
		.then(result => { 
			obj.deletedAt = new Date;
			return obj;
		});
	}







	/**
	 * Directory returns all ESCORT Personnel
	 * @returns {Array} List of all Personnel with ESCORT flag
	 */
	directory() {
		return this.find(Personnel.TYPE, {EscortOption:Personnel.ESCORT_OPTION.ESCORT});
	}

	findDefaultPartition() {
		if (this._default_partition) {
			return new Promise((resolve, reject) => {
				resolve(this._default_partition);
			});
		} else {
			return this.find(Partition, {IsDefaultPartition: true})
				.then(result => {
					this._default_partition = result[0];
					return this._default_partition;
				});
		}
	}



	/** Get the "SV3 Visitor" PersonnelType, or create it.
	 * New to C*CURE9000 v.2.50 (or 2.40?) is the PersonnelType with CanBeVisitor flag.
	 * "Visitors" in CCURE are Personnel with a PersonnelTypeID of a PersonnelType with the 
	 * CanBeVisitor flag set.
	 *
	 * @returns {Promise<PersonnelType>} The PersonnelType object to use for visitors 
	 */
	findSv3VisitorType() {
		log.debug("findSv3VisitorType() called");

		return new Promise((resolve, reject) => {
			if (this._SV3_VISITOR_PERSONNEL_TYPE) {
				log.debug({result: this._SV3_VISITOR_PERSONNEL_TYPE}, 'findSv3VisitorType() returning cached result.')
				resolve(this._SV3_VISITOR_PERSONNEL_TYPE);
			} else {
				log.debug('findSv3VisitorType() cache miss. Fetching from CCURE.');

				let template = PersonnelType.VISITOR_TEMPLATE;

				this.client.objects_get_all_with_criteria({
					TypeFullName: PersonnelType.TYPE,
					WhereClause: 'Name like ? and CanBeVisitor=?',
					Arguments: [template.Name, template.CanBeVisitor]
				})
				.then((types) => {
					if (types.length < 1) {
						log.debug('findSv3VisitorType() CCURE returned empty list.  Creating new Visitor Type.');
						//Empty results; let's create one.
						this.findDefaultPartition()
						.then((partition) => {
							template.PartitionID = partition.ObjectID;
							return template;
						})
						.then(() => {
							return this.save(template).then((savedObj) => {
								log.debug({result: savedObj}, 'findSv3VisitorType() newly saved CCURE Type for Visitor')
								this._SV3_VISITOR_PERSONNEL_TYPE = savedObj;
								resolve(savedObj);
							})
						})
						.catch(reject);
					} else {
						//TODO: refactor find operation to return type expected.
						resolve(new PersonnelType(types[0])); //Return first matching
					}
				})
				.catch(reject);
			}
		})
	}


	/** Finds a credential with matching CardNumber or creates a new one.
	 *
	 * DO NOT USE!!!
	 *
	 * FAULT/BROKEN: CCURE Does not appear to allow the creation of credentials w/o PersonnelID
	 *
	 *      { "Message": "AddingOfCredentialsNotAllowed" }
	 * 
	 * Possible remedey: use PersonnelID 1000.  This seems to be what CCURE does.
	 *
	 * @param {number} cardNumber - the desired CardNumber
	 * @returns {Promise<Credential>} - The CCURE credential
	 */ /*
	findOrCreateCredential(cardNumber) {
		return this.find(Credential, {CardNumber: cardNumber})
			.then(cards => {
				// Return the first match, if there are matches
				if (cards.length > 0) return cards[0];
				else {
					return this.save(
						new Credential({CardNumber: cardNumber})
					)
				}
			});
	}
	*/

	/** Find Or Create Visitor from a Shortpath Visit Object

	    Sample ShortpathVisitObject (received from AMQP json):

		{
			first_name: '',
			last_name: '',
			card: 123456,
			active_date: '', //string in format "%m/%d/%Y 00:00:00"
			expiry_date: '',
			clearance_ids: [5001],

			//New fields added to Shortpath v.2.5?.???
			email: 'visit.guest.email',
			guid: 'visit.guest.uuid',
			host: {
				first_name: 'host.first_name',
				last_name:  'host.last_name',
				guid: 		'host.uuid',
				email: 		'host.email || ""',
				login: 		'host.login',
				badge_id: 	'host.badge_id'
			}
			//temp_badge_for_user_guid: 'some uuid goes here', //This is only used for temp employee bagde. Ignore for now?
		}


	 * @param {Object} shortpathVisitObj - The deserialized JSON blob from Shortpath with visitor and host info
	 * @returns {Promise<Personnel>} - The found or created visitor Personnel object
	 */
	findOrCreateVisitor(shortpathVisitObj) {
		const [criteria, priority] = util.sanitize.sanitizeSearchCriteria({
			GUID: shortpathVisitObj.guid,
			EmailAddress: shortpathVisitObj.email,
			FirstName: shortpathVisitObj.first_name,
			LastName: shortpathVisitObj.last_name
		}, config.get('ccure:visitor:match'));

		log.trace({shortpathVisitObj, criteria, priority}, "#findOrCreateVisitor() called")

		return this.findSv3VisitorType()
		.then(visitorType => {
			return Promise.all([
				this.findFirstMatch(Personnel, criteria, priority, {PersonnelTypeID: visitorType.ObjectID}),
				visitorType
			])
		})
		.then(([visitor, visitorType]) => {
			// result should be [[visitors], PersonnelType<Visitor>], and visitors may be empty
			return (visitor) ? Promise.resolve(visitor) :
				this.save(new Personnel(
					//Remove any empty or null attributes
					_.pickBy({
						//TODO: Should PartitionID be explicitly specified here?
						FirstName: shortpathVisitObj.first_name,
						LastName: shortpathVisitObj.last_name,
						GUID: shortpathVisitObj.guid,
						PersonnelTypeID: visitorType.ObjectID,
						EscortOption: config.get('ccure:visitor:escort') || Personnel.ESCORT_OPTION.ESCORTED_VISITOR,
						EmailAddress: shortpathVisitObj.email
						//NOTE: Email is not guarenteed unique, may cause validation error and not save.
						//TODO: Handle non-unique email error; remove email attribute and try again.
						//TODO: Add support for visitor photo!
					}, function(v, k) { //Careful! _pickBy() calls f(value, key)!
						return (v);
					})
				));
		})
	}


	/* Check-in a visitor from Shortpath

	    Sample ShortpathVisitObject (received from AMQP json):

		{
			first_name: '',
			last_name: '',
			card: 123456,
			active_date: '', //string in format "%m/%d/%Y 00:00:00"
			expiry_date: '',
			clearance_ids: [5001],

			//New fields added to Shortpath v.2.5?.???
			email: 'visit.guest.email',
			guid: 'visit.guest.uuid',
			host: {
				first_name: 'host.first_name',
				last_name:  'host.last_name',
				guid: 		'host.uuid',
				email: 		'host.email || ""',
				login: 		'host.login',
				badge_id: 	'host.badge_id'
			}
			//temp_badge_for_user_guid: 'some uuid goes here', //This is only used for temp employee bagde. Ignore for now?
		}
	 *
	 * @param {Object} shortpathVisitObj - Object passed from Shortpath via AMQP.
	 * @returns {Promise<Visit>} The created Visit object, maybe.
	 **/
	checkin(shortpathVisitObj) {
		let visitor, host, cards, card, clearances, visit;

		//TODO: handle case where shortpathVisitObj is missing Visitor UUID and host information. (older versions)

		log.trace({obj:shortpathVisitObj}, "Searching for card, visitor, and host")

		const [criteria, priority] = util.sanitize.sanitizeSearchCriteria({
				GUID: shortpathVisitObj.host.guid,
				EmailAddress: shortpathVisitObj.host.email,
				FirstName: shortpathVisitObj.host.first_name,
				LastName: shortpathVisitObj.host.last_name
			}, config.get('ccure:host:match'))

		//Find the visitor, host, credential, and clearances, then put the pieces together
		return this.findSv3VisitorType()
		.then(visitorType => {
			return Promise.all([
				this.find(Credential, {CardNumber: shortpathVisitObj.card}),
				this.findOrCreateVisitor(shortpathVisitObj),
				
				//TODO: Sanitize host matching!!!!
				this.findFirstMatch(Personnel, criteria, priority, 
					["PersonnelTypeID != ?", visitorType.ObjectID]),
				(shortpathVisitObj.clearance_ids && shortpathVisitObj.clearance_ids.length > 0) ? 
				this.find(Clearance, {ObjectID: shortpathVisitObj.clearance_ids}) : []
			]);
		}).then(result => {
			log.debug({result: result}, "checkin: All Promises returned! (round 1)");

			[cards, visitor, host, clearances] = result;

			if (!host && config.get('ccure:visit:enabled')) {
				log.error("Host not found and required for Visit. Aborting.");
				throw new Error("Host was not found!");
			}
			if (!visitor) {
				log.error("Visitor not found and required! Aborting.");
				throw new Error("Visitor cannot be null!");
			} 

			//TODO: Don't delete card, just reassign it (when updating is supported)
			if (cards.length > 0) {
				return this.destroy(cards[0]);
			}
		}).then(() => {

			card = new Credential({ CardNumber: shortpathVisitObj.card });
			card.PersonnelID = visitor.ObjectID;
			card.ActivationDateTime = shortpathVisitObj.active_date;
			card.ExpirationDateTime = shortpathVisitObj.expiry_date;

			if (config.get('ccure:visit:enabled')) {
				visit = new Visit();
				visit.Visitors.push(visitor);
				visit.Hosts.push(host);
				//visit.Name = `${moment().format('YYYMMDD-HHmmss.SSS')} ${visitor.Name}  ${host.Name}`.substr(0,100);
			}

			return Promise.all([
				config.get('ccure:visit:enabled') ? this.save(visit) : false,
				this.save(card), //Fails when reusing credential
				visitor,
				host,
				clearances,
				this.assign(visitor, clearances.map(c => { //TODO: This causes 500 error when visitor already has the clearance(s)
					return new PersonnelClearancePair({
						PersonnelID: visitor.ObjectID,
						ClearanceID: c.ObjectID
					})
				}))
			]);
		})
		
	}


	get journal() {
		if (this._journal instanceof JournalPoller) return this._journal;
		return this._journal = new JournalPoller(this.client)
	}

}


module.exports = CcureService;