'use strict'

require('dotenv').config()
const Client = require('./Client.js');
const Service = require('./Service.js');
const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');
const Credential = require('./Credential.js');
const random = require('../util/random.js');
const _ = require('lodash')

const client = new Client();
const service = new Service(client);


function createPeople(quantity=30) {
	return Promise.all([
		service.findDefaultPartition(), 
		service.findSv3VisitorType()])
	.then(([partition, visitorType]) => {
		//Create some Personnel and Credentials
		return Promise.all(Array.from(new Array(quantity)).map(i => {
			//Create a Personnel with a Credential
			return service.save(new Personnel(new random({
				EscortOption:Personnel.ESCORT_OPTION.ESCORT,
				PartitionID: partition.ObjectID,
				PersonnelTypeID: 2
			})));
		}));
	});
}


/*

service.find(Personnel, {FirstName:'Franklin', LastName:'Funaro'})
.then(results => {
	console.log(results);
})
*/
/*
service.find(Personnel, ["FirstName = 'Franklin' AND LastName LIKE 'F%'"])
.then(results => {
	console.log(results);
})
*/
service.login()
.then((s) => {
	console.log("logged in.", s)
	return service.checkin({
		first_name: 'Bruno',
		last_name: 'Boehning',
		card: 1234567,
		active_date: '02/01/2018 06:00:00', //string in format "%m/%d/%Y 00:00:00"
		expiry_date: '02/01/2018 23:59:59',
		clearance_ids: [5001],

		//New fields added to Shortpath v.2.5?.???
		email: '',
		guid: '1ad689a0-5b67-43ae-824c-bf84f26b05a5',
		host: {
			first_name: 'Bruno',
			last_name:  'Boehning',
			guid: 		'7abcd18b-4334-4062-be38-63a0dd88d252',
			email: 		'Bruno.Boehning@lvh.me',
			login: 		'Bruno.Boehning',
			badge_id: 	'59384'
		}
		//temp_badge_for_user_guid: 'some uuid goes here', //This is only used for temp employee bagde. Ignore for now?
	})
}).then(result => {
	console.log(result);
}).catch(err => {
	console.log("Error!", err)
})


/*
service.findAll(PersonnelType, {})
.then(results => {
	console.log(results);
})

/* Give all non-visitors a credential */
/*
service.findAll(Personnel, {EscortOption: Personnel.ESCORT_OPTION.ESCORT})
.then(people => {
	console.log(`Found ${people.length} Personnel`);
	
	var working = true;
	for(let i=0; i<people.length && working; i++) {
		let card = new Credential(new random());
		card.Temporary = false
		card.Name = `Employee Badge ${card.CardNumber}`
		card.PersonnelID = people[i].ObjectID;
		service.save(card)
		.then(c => {
			console.log(`Saved Card ${card.CardNumber}`)
		})
		.catch(err => {
			console.log("Error!", err);
			working = false;
		})
	}
})
.catch(err => {
	console.log("Error!", err)
})
*/

/*

client.login(function(err){

	service.findAll(Personnel, {
		EscortOption: Personnel.ESCORT_OPTION.ESCORT
	}).then(people => {

		console.log(people.length)

		console.log(
			_.uniq( people.map(i => { return i.ObjectID }) ).length
		)
	}).catch(err => {
		console.log("Error!", err)
	});

})



/*
createPeople(10000).then(people => {

	console.log("Created 10000")

	/*
	let labrats = people.splice(random.cardNumber(0, people.length-3), 3);

	//Should return labrats in 2, 0 ,1 order.
	let p = service.findMultiple(Personnel, {
		GUID: labrats[2].GUID,
		EmailAddress: labrats[0].EmailAddress,
		FirstName: labrats[1].FirstName,
		LastName: labrats[1].LastName
	}, [['GUID'], ['EmailAddress'], ['FirstName', 'LastName']]);
	console.log(p);
	return p;
})
.then(results => {
	console.log(results);
	/*
	expect(results).to.be.an('array').lengthOf(3);
	//Expand and test.
	let [two, zero, one, ...other] = results;
	
	expect(zero).to.be.an('array').lengthOf(1);
	expect(one).to.be.an('array').lengthOf(1);
	expect(two).to.be.an('array').lengthOf(1);
	expect(other).to.be.empty();

	[two, zero, one] = [...two, ...zero, ...one];

	expect(two).to.have.property('GUID', labrats[2].GUID);
	expect(zero).to.have.property('EmailAddress', labrats[0].EmailAddress);
	expect(one).to.have.property('FirstName', labrats[1].FirstName);
	expect(one).to.have.property('LastName', labrats[1].LastName);
	*/
	/*
})
.catch(err => {
	console.log(err);
})
*/

/*
service.find(Credential, {ObjectID:5013})
.then((cards) => { console.log(cards) });
*/

/*
client.schema_get(Personnel.TYPE)
.then(result => { console.log(result); });
*/

//service.find(Personnel, {GUID: "388f8c22-867d-4550-9cf9-eba4d99115af"})
//service.find(Personnel, {ObjectID: 5382})
//service.find(Personnel, {GUID: '875c2d9d-9dfc-48d2-be78-9a1e8f71ed0a'})
//.then(result => { console.log(result) });

/*
let p1 = new Personnel(new random()), p2, p3, p4, p5;
console.log("p1", p1);

service.save(p1)
.then(p => {
	p2 = p;
	console.log("p2", p2);
	return service.find(Personnel, {ObjectID: p2.ObjectID})
}).then(p => {
	p3 = p[0];
	console.log("p3", p3);
	return service.find(Personnel, {GUID: p3.GUID})
}).then(p => {
	p4 = p;
	console.log("p4", p4);
//	return service.find(Personnel, {EmailAddress: p3.EmailAddress})
//}).then(p => {
//	p5 = p;
//	console.log("p5", p5)
}).catch(err => { console.log("Error!", err) });
*/