"use strict";

/* VisitHostPair Object */

const Ccure9kObject = require('./Ccure9kObject.js');
const _ = require('lodash');

const TYPE = 'SoftwareHouse.NextGen.Common.SecurityObjects.VisitHostPair'
const PROPERTIES = [
	'ObjectID',
	'VisitID',
	'PersonnelID',
	'Notify'
]
const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";

class VisitHostPair extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);

		//Fill in Defaults
		// _.defaults(this, {
		// 	Notify: true
		// });
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }

}

module.exports = VisitHostPair;
