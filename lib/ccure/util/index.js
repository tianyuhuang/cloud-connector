/* A home for CCURE dependent utility functions */

'use strict';
const sanitize = require('./sanitize.js');


module.exports = {
	sanitize
}