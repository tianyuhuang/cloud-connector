'use strict';

require('dotenv').config()
const nconf = require('nconf');
const uuid = require('uuid/v4');
const Personnel = require('./ccure/Personnel.js')


const DEFAULT_CONFIG = {
	app: {
		code: uuid()  //Generate new UUID
	},
	mq: {
		host: 'localhost',
		login: 'guest',
		password: 'guest',
		vhost: 'ccure',
		authMechanism: "AMQPLAIN",
		ssl: { 
			enabled: false, 
			rejectUnauthorized: false 
		},
		connectionTimeout: 10000,
		heartbeat: 30,
		defaultExchangeName: "cc2sv3"
	},
	ccure: {
		enabled: false,
		connection: {
			host: "localhost",
			username: "SV3",
			password: "password",
			clientname: "SV3 Cloud Connector",
			keepalive: 65000 //sessionRefreshInterval: 85000
		},
		polling: {
			enabled: false,
			fetchCardDuration: 1,
			fetchCardInterval: 2000,
			fetchCardEndFromNow: 2,
		},
		host: {
			match: [['GUID'], ['EmailAddress'], ['LastName', 'FirstName']]
		},
		visitor: {
			match: [['GUID'], ['EmailAddress'], ['LastName', 'FirstName']],
			escort: Personnel.ESCORT_OPTION.ESCORTED_VISITOR
		},
		visit: {
			enabled: false
		}
	},
	"genetec": {
    "enabled": false,
    "webSDK": {
      "enabled": true,
      "protocal": "http",
      "server": "localhost",
      "port": "4590",
      "baseUri": "WebSdk",
      "username": "Admin",
      "password": "shortpath",
      "applicationId": "KxsD11z743Hf5Gq9mv3+5ekxzemlCiUXkTFY5ba1NOGcLCmGstt2n0zYE9NsNimv",
      "timeBetweenRequests": 150 // slowdown requests since Genetec SDK cannot handle concurrency well
    },
    "lprSDK": {
      "enabled": true,
      "lprDir": "C:\\Users\\Administrator\\Desktop\\Dev\\code\\lprDir"
    },
    "visitor": {
      "facilityCode": "1"
    },
    "cardholder": {
      "fieldCompanyName": "CardholderGroup.Name", // CardholderGroup.Name || Cardholder.Description || Cardholder.CustomFields.CompanyName
      "fieldCompanyFloor": "Cardholder.CustomFields.Floor" // Group.CustomFields.Floor || Cardholder.CustomFields.Floor
    }
	},
	socket: {
		url: "https://cloud_notifier.shortpath.com"
	},
	config: {
		log: {
			level: 'info',
			period: '1h',
			count: 12,
			format: 'json',
			gzip: true,
			threshold: '100m',
			totalSize: '200m'
		}
	},
	env: 'production'
}

const yargs_option = {
	'v': {
		alias: 'version',
		describe: 'Print the version and exit',
		demand: false
	},
	'c': {
		alias: 'configure',
		describe: 'Enter interactive configuration mode',
		demand: false
	},
	'e': {
		alias: 'environment',
		describe: 'Set the runtime environment [development, staging, production]',
		demand: false
	},
	'L': {
		alias: 'log',
		describe: 'Pipe Bunyan JSON log to stdout (pretty formatted). Optionally specify level.',
		demand: false,
		default: false
	},
	'd': {
		alias: 'certificates',
		describe: 'Dump all certificates to stdout',
		demand: false
	},
	's': {
		alias: 'show',
		describe: 'Show all configuration parameters to stdout and exit',
		demand: false
	}
}

nconf.use('memory');
nconf.argv(yargs_option)
	.env({
		separator: '__',
		whitelist: ['node_env', 'ccure_host', 'ccure_username', 'ccure_password', 
			'mq_host', 'mq_login', 'mq_password', 'mq_vhost'],
		lowerCase: true
	})
	.file({ file: 'config.json' });

nconf.defaults(DEFAULT_CONFIG);

module.exports = nconf;