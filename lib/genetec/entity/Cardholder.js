const Promise = require('bluebird') //Promise.map is available in bluebird

const Entity = require('./Entity')
const nconf = require('../../config.js')

class Cardholder extends Entity {

  /*
    Class initializer

    @param {object} params - object attributes
      example object
      {
        logicalId: '', 
        firstName: '', 
        lastName: '',
        emailAddress: ''      
      }
    @param {object} opts - additional options(currently not being used)
  */
  constructor(params, opts){
    super(params, opts)
    this.entityType = "Cardholder"
  }

  /*
    Get all cardholder data
    
    @return {promise[array]} array of cardholder objects
      sample return:
      [{
        firstName: '',
        lastName: '',
        email: '',
        guid: '',
        companyName: '',
        floor: '',
        logicalId: ''
      }]
  */
  static async getList(){
    return Cardholder.getListGuids().then(Cardholder.getListByGuids)
  }

  /*
    Get all cardholder guids

    @return {promise[array]} array of guid string
      sample return:
      ['guid1', 'guid2']
  */
  static async getListGuids(){
    return Entity.listAllByType("Cardholder").then((r) => {
      let guids = []
      let rows = r.rsp.QueryResult[0].Row
      for (var r of rows) {
        r.Cell.forEach((c) => {
          if(c['$'].name == 'Guid'){
            guids.push(c['_'])
          }
        })
      }

      return guids
    })
  }

  /*
    Get cardholder data from list of guids

    @param {array} guids - list of guids
    @return {promise[array]} array of cardholder objects
      sample return:
      [{
        firstName: '',
        lastName: '',
        email: '',
        guid: '',
        companyName: '',
        floor: '',
        logicalId: ''
      }]
  */
  static async getListByGuids(guids){
    let promises = Promise.map(
      guids, 
      guid => {
        return Entity.findByGuid(guid).then(async (r) => {
          let user = Cardholder.parseCardholder(r)
          return user
        })
      }, 
      { concurrency: 1 }
    )

    return promises
  }

  /*
    Get cardholder data for a guid

    @param {string} guid - guid
    @return {json} cardholder objects
      sample return:
      {
        firstName: '',
        lastName: '',
        email: '',
        guid: '',
        companyName: '',
        floor: '',
        logicalId: ''
      }
  */
  static async getSingleByGuid(guid){
    let obj = await Entity.findByGuid(guid).then(async (r) => {
      let user = Cardholder.parseCardholder(r)
      return user
    }).catch(err => {
      console.log("=== Error: get cardholder for GUID: ", guid)
    })

    return obj
  }


  static parseCardholder(xmlObj) {
    let fieldCompanyName = nconf.get('genetec:cardholder:fieldCompanyName')

    let d = xmlObj.rsp.entity[0];

    // Get custom fields
    let companyName = null
    let floor = null
    let email = d.EmailAddress[0]
    let guid = d.Guid[0]

    if(d.CustomFields){
      d.CustomFields.forEach((e) => {
        if(e.item){
          e.item.forEach((item) => {
            let i = item.CustomFieldValue[0]
            let customFieldName = i.CustomField[0].Name[0].toLowerCase()
            // Cardholder.CustomFields.CompanyName
            if(customFieldName == "companyname"){
              companyName = i.Value[0]
            }

            // Cardholder.CustomFields.Floor
            if(customFieldName == "floor"){
              floor = i.Value[0]
            }
          })
        }
      })
    }

    if(fieldCompanyName == "Cardholder.Description"){
      companyName = d.Description[0]
      if(companyName){
        companyName = companyName.split("-")[0] // remove after -
      }
    }

    if(companyName){
      companyName = companyName.replace(/\"/g,"") // remove all "
      companyName = companyName.trim() // remove begining trailing space
    }

    if(!email){
      email = `guid-${guid}@sv3-visitor.com`
    }

    let user = {
      firstName: d.FirstName[0],
      lastName: d.LastName[0],
      email: email,
      guid: guid,
      companyName: companyName,
      floor: floor,
      logicalId: d.LogicalId[0]
    }
    return user
  }
  
}

module.exports = Cardholder
