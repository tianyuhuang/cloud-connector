const Entity = require('./Entity')
const Cardholder = require('./Cardholder')
const Client = require('../lib/Client')

class CardholderGroup extends Entity {

  /*
    Class initializer

    @param {object} params - object attributes
      example object
      {
        logicalId: '', 
        name: '', 
        isVisitorGroup: true  
      }
    @param {object} opts - additional options(currently not being used)
  */
  constructor(params, opts){
    super(params, opts)
    this.entityType = "CardholderGroup"
  }

  static getMemberIdsFromGroupHash(group_hash){
    let result = []
    let g = group_hash
    let entity = g.rsp.entity[0]
    let members = entity.Members[0]
    if(members){
      let items = members.item
      if(items){
        items.forEach((i) => {
          let mGuid = i.Guid[0]
          result.push(mGuid)
          
        })
      }
      
    }
    return result
  }

  /*
    Find a group named "Visitors" and has IsVisitorGroup set to "True"

    @return {promise}
  */
  static async findAllGroups(opts={isVisitorGroup: false, includeMemberIDs: false, includeMembers: false}){
    let path = "report/EntityConfiguration"
    let p = {"q": "EntityTypes@CardholderGroup"}
    let self = this

    let resp = await Client.get(path, p)

    // find all guids
    let guids = [];
    resp.rsp.QueryResult[0].Row.forEach((r) => {
      r.Cell.forEach((c) => {
        if(c['$'].name == 'Guid'){
          guids.push(c['_'])
        }
      })
    })

    // let array = guids.map(async(gid)=>{
    let array = []


    for(var i in guids){
      let gid = guids[i]

      let g = await self.findByGuid(gid)

      let entity = g.rsp.entity[0]
      let ok = false

      // check
      if(opts.isVisitorGroup){
        if(entity.IsVisitorGroup == 'True'){
          ok = true
        }
      }
      else{
        if(entity.IsVisitorGroup == 'False'){
          ok = true
        }
      }

      // parse obj
      let groupObj = {
        id: entity.Guid[0],
        guid: entity.Guid[0],
        name: entity.Name[0],
        floor: null,
        members: []
      }

      if(ok){
        // get custom fields
        if(entity.CustomFields){
          entity.CustomFields.forEach((e) => {
            if(e.item){
              e.item.forEach((item) => {
                let i = item.CustomFieldValue[0]
                let customFieldName = i.CustomField[0].Name[0].toLowerCase()
                if(customFieldName == "floor"){
                  groupObj.floor = i.Value[0]
                }
              })
            }
          })
        }

        // get members
        if(opts.includeMemberIDs || opts.includeMembers){
          let memberIds = self.getMemberIdsFromGroupHash(g)
          if(opts.includeMemberIDs){
            groupObj.memberIDs = memberIds
          }
          if(opts.includeMembers){
            let members = await Cardholder.getListByGuids(memberIds)
            groupObj.members = members
          }
        }
      }
    
      if(ok){
        array.push(groupObj)
      }
    }



    array = array.filter(x => !!x)
    return array
  }

  static async findAllGroupsAsync(opts={isVisitorGroup: false, includeMemberIDs: false, includeMembers: false}){
    let path = "report/EntityConfiguration"
    let p = {"q": "EntityTypes@CardholderGroup"}
    let self = this

    let promise = new Promise((resolve, reject) => {

      Client.get(path, p).
        then(async(resp)=>{
          // find all guids
          let guids = [];
          resp.rsp.QueryResult[0].Row.forEach((r) => {
            r.Cell.forEach((c) => {
              if(c['$'].name == 'Guid'){
                guids.push(c['_'])
              }
            })
          })

          let array = await Promise.all(guids.map(async(gid)=>{
            let g = await self.findByGuid(gid)

            let entity = g.rsp.entity[0]
            let ok = false

            // check
            if(opts.isVisitorGroup){
              if(entity.IsVisitorGroup == 'True'){
                ok = true
              }
            }
            else{
              if(entity.IsVisitorGroup == 'False'){
                ok = true
              }
            }

            // parse obj
            let groupObj = {
              id: entity.Guid[0],
              guid: entity.Guid[0],
              name: entity.Name[0],
              floor: null,
              members: []
            }


            if(ok){
              // get custom fields
              if(entity.CustomFields){
                entity.CustomFields.forEach((e) => {
                  if(e.item){
                    e.item.forEach((item) => {
                      let i = item.CustomFieldValue[0]
                      let customFieldName = i.CustomField[0].Name[0].toLowerCase()
                      if(customFieldName == "floor"){
                        groupObj.floor = i.Value[0]
                      }
                    })
                  }
                })
              }

              // get members
              if(opts.includeMemberIDs || opts.includeMembers){
                let memberIds = self.getMemberIdsFromGroupHash(g)
                if(opts.includeMemberIDs){
                  groupObj.memberIDs = memberIds
                }
                if(opts.includeMembers){
                  let members = await Cardholder.getListByGuids(memberIds)
                  groupObj.members = members
                }
              }
            }

            return ok ? groupObj : null
          }))

          array = array.filter(x => !!x)

          resolve(array)
        })
    })

    return promise
  }


  /*
    Find a group by given name

    @param {string} groupName

    @return {promise[object]} group object
      example return:
      {
        guid: '',
        logicalId: '',
        name: ''
      }
  */
  static async findGroupByName(groupName){
    let path = "report/EntityConfiguration"
    let p = {"q": "EntityTypes@CardholderGroup"}

    let promise = new Promise((resolve, reject) => {

      Client.get(path, p).
        then((resp)=>{
          // find all guids
          let guids = [];
          resp.rsp.QueryResult[0].Row.forEach((r) => {
            r.Cell.forEach((c) => {
              if(c['$'].name == 'Guid'){
                guids.push(c['_'])
              }
            })
          })

          // find visitors guid
          guids.forEach((g) => {
            this.findByGuid(g).
              then(resp => {
                let entity = resp.rsp.entity[0]
                // console.log("[Group] ", entity)
                if(entity.Name[0] == groupName){
                  let r = {
                    guid: g,
                    logicalId: entity.LogicalId[0],
                    name: entity.Name[0]
                  }
                  resolve(r);
                }
                
              })
          })


        })
    })

    return promise
  }

  /*
    Add a cardholder to a group

    @param {string} cardholderType - can be "Visitor" or "Cardholder"
    @param {string} cardholderLogicalIdOrGuid
    @param {string} groupLogicalIdOrGuid

    @return {promise}
  */
  static async addCardholderToGroup(cardholderType, cardholderLogicalIdOrGuid, groupLogicalIdOrGuid){

    //
    let promiseFindCardholderGuid = new Promise((resolve, reject) => {
      let cardholderGuid = null

      if(cardholderLogicalIdOrGuid.length >= 32){
        cardholderGuid = Entity.normalizeGuid(cardholderLogicalIdOrGuid)
        resolve(cardholderGuid)
      }
      else{
        Entity.findByLogicalId(cardholderType, cardholderLogicalIdOrGuid).then((r)=>{
          cardholderGuid = r.rsp['entity'][0]['Guid'][0]
          cardholderGuid = Entity.normalizeGuid(cardholderGuid)
          resolve(cardholderGuid)
        })
      }
    })

    //
    let promiseFindGroupGuid = new Promise((resolve, reject) => {
      let groupGuid = null

      if(groupLogicalIdOrGuid.length >= 32){
        groupGuid = Entity.normalizeGuid(groupLogicalIdOrGuid)
        resolve(groupGuid)
      }
      else{
        Entity.findByLogicalId("CardholderGroup", groupLogicalIdOrGuid).then((r)=>{
          groupGuid = r.rsp['entity'][0]['Guid'][0]
          groupGuid = Entity.normalizeGuid(groupGuid)
          resolve(groupGuid)
        })
      }
    })

    //
    return Promise.all([promiseFindCardholderGuid, promiseFindGroupGuid]).then((r) => {
      let cardholderGuid = r[0]
      let groupGuid = r[1]

      let path = "entity"
      let p = {
        "q": `entity=${groupGuid}`,
        "Members@": `{${cardholderGuid}}`
      }

      return Client.post(path, p)
    })
  }
  
}

module.exports = CardholderGroup
