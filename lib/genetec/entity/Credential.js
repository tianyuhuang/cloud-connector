const Entity = require('./Entity')
const Client = require('../lib/Client')

const nconf = require('../../config.js')

class Credential extends Entity {

  /*
    Class initializer

    A credential is a card with number, 
    one credentnial can only be assigned to one user.

    @param {object} params - object attributes
      example object
      {
        logicalId: '', 
        name: '', 
        facilityCode: '',
        cardNumber: ''      
      }
    @param {object} opts - additional options(currently not being used)
  */
  constructor(params, opts){
    super(params, opts)
    this.entityType = "Credential"
    if(!this.params.facilityCode){
      let visitorFacilityCode = nconf.get('genetec:visitor:facilityCode')
      this.params.facilityCode = visitorFacilityCode
    }
  }

  /*
    Create this credential

    @return {promise}
  */
  async create(){
    let path = "entity"
    let params1 = {
      "q": `entity=NewEntity(${this.entityType})`
    }
    let params2 = {
      logicalId: this.params.logicalId,
      name: this.params.name,
      format: `WiegandStandardCredentialFormat(${this.params.facilityCode},${this.params.cardNumber})`
    }
    // let p = {...params1, ...params2}
    let p = Object.assign({}, params1, params2)
    return Client.post(path, p)
  }


  /*
    Attach a credential to a visitor

    @param {string} credentialLogicalId
    @param {string } visitorLogicalId

    @return {promise}
  */
  static async addCredentialToVisitor(credentialLogicalId, visitorLogicalId){
    let path = "entity"
    let p = {
      "q": `entity=LogicalId(Visitor,${visitorLogicalId})`,
      "Credentials@": `LogicalId(Credential,${credentialLogicalId})`
    }
    return Client.post(path, p)
  }

  
}

module.exports = Credential
