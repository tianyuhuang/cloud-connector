const chai = require('chai')
const expect = chai.expect
const Credential = require('./Credential')
const fs = require('fs')
const path = require('path')
const nock = require('nock')
const xml2js = require('xml2js-parser').parseStringSync

process.env.NODE_ENV='test'

describe('Credential', () => {

  beforeEach(() => {
    let reqheaders = {}

    let addCredToVisitorResp = xml2js(`
      <rsp status="ok">
      </rsp>`)

    nock('http://localhost:4590',{reqheaders: reqheaders})
      .post('/WebSdk/entity?q=entity=LogicalId(Visitor,1111),Credentials@LogicalId(Credential,1111)')
      .reply(200, addCredToVisitorResp)
  });
 
  describe('#addCredentialToVisitor', () => {
    it('should return success', () => {
      Credential.addCredentialToVisitor(1111, 1111).then((r) => {
        expect(r.rsp['$'].status).to.equal('ok')
      })
    })
  })

  
})