const Client = require('../lib/Client')

class CustomField {

  constructor(){}

  // field = {name: "", type: ""}
  static async createFieldForEntity(entity_name, field){
    let path = `customField/${entity_name}/${field.name}/${field.type}/`
    return Client.post(path)
  }

  // field = {name: ""}
  static async deleteFieldForEntity(entity_name, field){
    let path = `customField/${entity_name}/${field.name}`
    return Client.delete(path)
  }

}

module.exports = CustomField