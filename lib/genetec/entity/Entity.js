const Client = require('../lib/Client')

/*
  Parent class of all Genetec Entity
*/
class Entity {
  constructor(params, opts){
    this.params = params
    this.opts = opts
  }

  /*
    Find an Entity by given guid

    @param {string} guid

    @return {promise}
  */
  static async findByGuid(guid){
    let path = `entity/${guid}`
    let p = {}
    return Client.get(path, p)
  }

  /*
    Find an Entity by given logicalId

    @param {string} entityType
    @param {string} logicalId

    @return {promise}
  */
  static async findByLogicalId(entityType, logicalId){
    let path = `entity/LogicalId(${entityType},${logicalId})`
    let p = {}
    return Client.get(path, p)
  }

  /*
    List all Entities by given type

    @param {string} entityType
    @param {int} limit - number of items per page
    @param {int} page - page number

    @return {promise}
  */
  static async listAllByType(entityType, limit, page){
    if(!limit) limit = 10000
    if(!page) page = 1
    let path = "report/EntityConfiguration"
    let p = {
      "q": `EntityTypes@${entityType}`,
      "PageSize": limit,
      "Page": page
    }
    return Client.get(path, p)
  }

  /*
    Convert given guid to standard format (hyphenated)

    @param {string} guid

    @return {string} hyphenated guid
  */
  static normalizeGuid(guid){
    if(guid.length == 32){
      return `${guid.slice(0, 8)}-${guid.slice(8, 12)}-${guid.slice(12, 16)}-${guid.slice(16, 20)}-${guid.slice(20,32)}`
    }
    else if(guid.length == 36){
      return guid
    }
    else{
      return null
    }
  }

  static timeout(ms){
    return Client.timeout(ms)
  }

  isNew(){
    return !this.guid
  }

  async save(){
    if(this.isNew()){
      return this.create()
    }
    else{
      return this.update()
    }
  }

  /*
    Create this entity

    @return {promise}
  */
  async create(){
    // console.log(`Create ${this.entityType}: ${this.params.logicalId}`)

    let path = "entity"
    let params1 = {
      "q": `entity=NewEntity(${this.entityType})`
    }
    let p = Object.assign({}, params1, this.params)
    return Client.post(path, p)
  }

  async update(){
  }

  async delete(){
    // console.log(`Delete ${this.entityType}: ${this.params.logicalId}`)

    let path = null
    if(this.params.guid){
      path = `entity/${this.params.guid}`
    }
    else if(this.params.logicalId){
      path = `entity/LogicalId(${this.entityType},${this.params.logicalId})`
    }
    
    return Client.delete(path)
  }

  /*
    Create this entity, if logicalId already exist, delete existing Entity then create a new one

    @return {promise}
  */
  async forceCreate(){
    let self = this;
    if(self.params.guid || self.params.logicalId){
      return this.delete().then(()=>{this.create()})
    }
    else{
      return this.create()
    }
  }

  baseUrl(){
    "entity/"
  }
  
}

module.exports = Entity

