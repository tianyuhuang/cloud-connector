const moment = require('moment')

const Entity = require('./Entity')
const Client = require('../lib/Client')
const Credential = require('./Credential')
const CardholderGroup = require('./CardholderGroup')

class Visitor extends Entity {

  /*
    Class initializer

    @param {object} params - object attributes
      example object
      {
        logicalId: '', 
        firstName: '', 
        lastName: '',
        emailAddress: '', 
        departure: '2018-02-12T20:31:00.02-05:00'
      }
    @param {object} opts - additional options(currently not being used)
  */
  constructor(params, opts){
    super(params, opts)
    this.entityType = "Visitor"
  }

  /*
    Check in visitor

    @param {object} json - visitor object received from sv3 through amqp
      example object
      {
        first_name: '',
        last_name: '',
        card: 123456,
        active_date: '', //unix time stamp
        expiry_date: '', //unix time stamp
        email: 'visit.guest.email',
        guid: 'visit.guest.uuid',
        clearance_ids: [] //list of group ids to add to
      }
    @return {promise}
  */
  static async checkIn(json){
    console.log("=== CheckIn | visitor: ", json)
    //time format example: 2018-02-12T20:31:00.02-05:00
    let dateEnd = parseInt(json.expiry_date)*1000
    let dateStart = parseInt(json.active_date)*1000
    let d_end = moment(dateEnd).format('YYYY-MM-DDTHH:mm:ssZ')
    let d_start = moment(dateStart).format('YYYY-MM-DDTHH:mm:ssZ')
    let visitorJson = {
      logicalId: `1${json.card}`,
      firstName: json.first_name,
      lastName: json.last_name,
      emailAddress: json.email,
      departure: d_end,
      arrival: d_start
    }

    let credentialJson = {
      logicalId: json.card, 
      name: "VisitorCredential", 
      cardNumber: json.card
    }

    console.log("=== CheckIn | create credential: ", credentialJson)
    // let p1 = CardholderGroup.findGroupByName(visitorGroupName)
    await (new Visitor(visitorJson)).forceCreate()
    await (new Credential(credentialJson)).forceCreate()


      // let visitorGroup = r[0]
      await Credential.addCredentialToVisitor(credentialJson.logicalId, visitorJson.logicalId)
      // await CardholderGroup.addCardholderToGroup("Visitor", visitorJson.logicalId, visitorGroup.guid)
      if(json.clearance_ids){
        json.clearance_ids.forEach(async(group_id) => {
          await CardholderGroup.addCardholderToGroup("Visitor", visitorJson.logicalId, group_id)
        })
      }

    return null
  }

  /*
    Check out visitor

    @param {object} json - visitor object received from sv3 through amqp
      example object
      {
        card: 123456,
      }
    @return {promise}
  */
  static async checkOut(json){
    let visitorJson = {
      logicalId: `1${json.card}`
    }

    let credentialJson = {
      logicalId: json.card
    }

    await (new Visitor(visitorJson)).delete()
    await (new Credential(credentialJson)).delete()

    return null
  }
  
}

module.exports = Visitor
