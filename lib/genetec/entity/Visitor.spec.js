const chai = require('chai')
const expect = chai.expect
const Visitor = require('./Visitor')
const fs = require('fs')
const path = require('path')
const nock = require('nock')
const xml2js = require('xml2js-parser').parseStringSync

process.env.NODE_ENV='test'


describe('Visitor', () => {

  beforeEach(() => {
    let reqheaders = {}

    let findGroupsResp = xml2js(`
      <rsp status="ok">
        <QueryResult>
          <HeaderRow>
            <Cell>
              <HeaderColumnName>Guid</HeaderColumnName>
              <Type>System.Guid</Type>
            </Cell>
          </HeaderRow>
          <Row>
            <Cell name="Guid">0000000000000000000000000000000d</Cell>
          </Row>
        </QueryResult>
      </rsp>`)

    let findGroupResp = xml2js(`
      <rsp status="ok">
        <entity>
          <EmailAddress></EmailAddress>
          <ActiveDirectoryDomainName></ActiveDirectoryDomainName>
          <IsVisitorGroup>True</IsVisitorGroup>
          <Members></Members>
          <Application>00000000000000000000000000000000</Application>
          <Behaviors></Behaviors>
          <EventToActions></EventToActions>
          <CreatedOn>2017-06-29T01:59:33</CreatedOn>
          <CustomFields></CustomFields>
          <Description></Description>
          <EntitySubType>1</EntitySubType>
          <EntityType>CardholderGroup</EntityType>
          <Guid>0000000000000000000000000000000d</Guid>
          <HierarchicalChildren></HierarchicalChildren>
          <HierarchicalParents></HierarchicalParents>
          <IsDisposed>False</IsDisposed>
          <IsOnline>True</IsOnline>
          <LinkedMaps></LinkedMaps>
          <LogicalId></LogicalId>
          <Name>Visitors</Name>
          <OwnerRole>00000000000000000000000000000000</OwnerRole>
          <OwnerRoleType>None</OwnerRoleType>
          <RunningState>Running</RunningState>
          <SupportedEvents>
            <item>
              <EventType>None</EventType>
            </item>
          </SupportedEvents>
          <Synchronised>False</Synchronised>
        </entity>
      </rsp>`)

    nock('http://localhost:4590',{reqheaders: reqheaders})
      .get('/WebSdk/report/EntityConfiguration?q=EntityTypes@CardholderGroup')
      .reply(200, findGroupsResp)


    nock('http://localhost:4590',{reqheaders: reqheaders})
      .get('/WebSdk/entity/0000000000000000000000000000000d?')
      .reply(200, findGroupResp)

    // delete old visitor
    nock('http://localhost:4590',{reqheaders: reqheaders})
      .delete('/WebSdk/entity/LogicalId(Visitor,123456)?')
      .reply(200, ``)

    // create new visitor
    nock('http://localhost:4590',{reqheaders: reqheaders})
      .post('/WebSdk/entity?q=entity=NewEntity(Visitor),logicalId=123456,firstName=TestF,lastName=TestL,emailAddress=test@test.com,departure=2018-03-04T20:00:00')
      .reply(200, ``)

    // delete old credential
    nock('http://localhost:4590',{reqheaders: reqheaders})
      .delete('/WebSdk/entity/LogicalId(Credential,123456)?')
      .reply(200, ``)

    // create new credential
    nock('http://localhost:4590',{reqheaders: reqheaders})
      .post('/WebSdk/entity?q=entity=NewEntity(Credential),logicalId=123456,name=VisitorCredential,format=WiegandH10304CredentialFormat(1,123456)')
      .reply(200, ``)

    // associate credential to visitor


    // add visitor to group
  });
 
  // describe('#checkin(json, visitorGroupName)', () => {
  //   it('should create a visitor', () => {
  //     let json = {
  //       first_name: 'TestF',
  //       last_name: 'TestL',
  //       card: 123456,
  //       active_date: '03/03/2018 20:00:00.00',
  //       expiry_date: '03/04/2018 20:00:00.00',
  //       email: 'test@test.com'
  //     }
  //     Visitor.checkIn(json, 'Visitors').then((res) => {
  //       console.log("Cannot test this, nested async promise")
  //     })
  //   })
  // })

  describe('Test async promise', () => {
    //==================================
    // non working example
    var rp = require('request-promise')
    var p1 = rp.get("https://www.google.com").promise()
    var p2 = new Promise(function(r) {
      p1.then(function(){
        p22 = rp.get("https://www.google.com").promise()
        r(p22)
      })
    })
    Promise.all([p1, p2]).then(function(r){
      console.log("this async promise only works in non test env")
    })
    //==================================
    // working example
    var promise1 = Promise.resolve(111);
    var promise2 = new Promise(function(resolve, reject) {
      resolve(222)
    });
    Promise.all([promise1, promise2]).then(function(r){
      console.log("this promise works in all env")
    })
    //==================================
  })
  
})


// // Import chai.

// // Import the classes
// // import { Visitor } from '../entity/visitor'
// // import { Credential } from '../entity/credential'
// // import { CardholderGroup } from '../entity/cardholder_group'

// const Visitor = require('../entity/Visitor')
// const Credential = require('../entity/Credential')
// const CardholderGroup = require('../entity/CardholderGroup')

// //
// let visitorJ = {
//   logicalId: "40001",
//   departure: "2018-02-12T20:31:00.02-05:00",
//   firstName: "TestF",
//   lastName: "TestL",
//   emailAddress: 'test@test.com'
// }

// let credentialJ = {
//   logicalId: "50001", 
//   name: "TestVisitorCredential", 
//   cardNumber: "50001"
// }

// let visitorGroupJ = {
//   logicalId: "400",
//   name: "TestVisitorGroup",
//   isVisitorGroup: "True"
// }

// //
// let p1 = (new CardholderGroup(visitorGroupJ)).forceCreate()
// let p2 = (new Visitor(visitorJ)).forceCreate()
// let p3 = (new Credential(credentialJ)).forceCreate()

// Promise.all([p1, p2, p3]).then((r) => {
//   let p4 = Credential.addCredentialToVisitor(credentialJ.logicalId, visitorJ.logicalId)
//   let p5 = CardholderGroup.addCardholderToGroup("Visitor", visitorJ.logicalId, visitorGroupJ.logicalId)
// })
