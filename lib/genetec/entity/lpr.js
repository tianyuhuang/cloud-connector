const fs = require('fs')
const path = require('path')
const xml2js = require('xml2js-parser').parseStringSync

class GenetecLPR{
  
  /*
    Parse lpr stream and convert it to json

    @param {stream object} stream

    @return {object}
      example return:
      {
        lpr_type: 'genetec',
        camera_name: '',
        license_plate_state: '',
        license_plate_number: '',
        car_image: '',
        plate_image: '',
        read_datetime: '',
        payload: {}
      }
  */
  static parseXML(stream){
    let xmlStr = stream.toString()
    // let jsonStr = xml2js.toJson(xmlStr)
    let json = xml2js(xmlStr)
    // let json = JSON.parse(jsonStr)
    let read = json.Read

    let dataToUpload = {
      "lpr_type": 'genetec',
      "camera_name": read.SharpName[0],
      "license_plate_state": read.State[0],
      "license_plate_number": read.Plate[0],
      "car_image": read.ContextImage[0],
      "plate_image": read.PlateImage[0],
      "read_datetime": read.Date[0] + " " + read.Time[0]
    };
    let msg = {
      camera: dataToUpload.camera_name, 
      plate_state: dataToUpload.license_plate_state,
      plate_number: dataToUpload.license_plate_number
    };
    console.log(msg);

    delete(read.PlateImage)
    delete(read.ContextImage)
    json.read = read
    delete(json.Read)

    dataToUpload.payload = json

    return dataToUpload
  }


}

module.exports = GenetecLPR
