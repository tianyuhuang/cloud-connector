const chai = require('chai');
const expect = chai.expect;
const GenetecLPR = require('./LPR')
const fs = require('fs')
const path = require('path')

describe('GenetecLPR', function(){
 
  describe('#parseXML(stream)', function(){
    it('should return correct license plate data', function(){
      let sampleXML = "docs/genetec/samples/lpr-sample-01.xml"
      let stream = fs.readFileSync(sampleXML)
      let json = GenetecLPR.parseXML(stream)
      expect(json.license_plate_number).equal('137XDD')
    })
  })
  
})