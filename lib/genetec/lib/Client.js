const URL = require('url')
const request = require('request')
const rp = require('request-promise')
const Promise = require('bluebird')
const xml2js = require('xml2js-parser').parseStringSync;

const nconf = require('../../config.js')
const config = nconf.get('genetec:webSDK')

class Client {

  /*
    Get auth object

    @return {object}
      example return
      {
        user: '',
        pass: ''
      }
  */
  static getAuth(){
    let u = config.username + ";" + config.applicationId
    let p = config.password
    return {user: u, pass: p}
  }

  /*
    Get webSDK url

    @return {string}
  */
  static getBaseUrl(){
    let url = `${config.protocal}:\/\/${config.server}:${config.port}\/${config.baseUri}\/`
    return url
  }

  /*
    Convert params object to url string, {key: val} -> uri + '?key=val,key2=val2'

    @param {object} params

    @return {string}
  */
  static genParamsString(params){
    let paramsArray = []
    for(var k in params){
      let v = params[k]
      if (k.endsWith("@")){
        paramsArray.push(`${k}${v}`)
      }
      else{
        paramsArray.push(`${k}=${v}`)
      }
    }
    return paramsArray.join(",")
  }

  /*
    Parse xml to json if response is of xml type
  */
  static autoParse(body, response, resolveWithFullResponse) {
    if (response.headers['content-type'] === 'application/xml') {
      // console.log(body)
      return xml2js(body)
    } else {
      return body
    }
  }

  /*
    Send GET request
  
    @param {string} path
    @param {object} params

    @return {promise}
  */
  static async get(path, params){
    // get uri with params
    let uri = URL.resolve(this.getBaseUrl(), path)
    uri += `?${this.genParamsString(params)}`
    console.log(`GET: ${uri}`)

    // send request
    let options = {
      method: 'GET',
      uri: uri,
      auth: this.getAuth(),
      qs: null,
      headers: null,
      json: process.env.NODE_ENV=='test' ? true : false, // Automatically parses the JSON string in the response
      transform: this.autoParse
    };

    let result = rp(options).promise()

    let timeBetweenRequests = parseInt(config.timeBetweenRequests) || 150
    await Client.timeout(timeBetweenRequests) // slow down for Genetec SDK since it cannot handle concurrency well

    return result
  }

  /*
    Send POST request
  
    @param {string} path
    @param {object} params

    @return {promise}
  */
  static async post(path, params){
    let uri = URL.resolve(this.getBaseUrl(), path)
    if(params){
      uri += `?${this.genParamsString(params)}`
    }
    console.log(`POST: ${uri}`)

    let options = {
      method: 'POST',
      uri: uri,
      auth: this.getAuth(),
      body: null,
      headers: null,
      json: process.env.NODE_ENV=='test' ? true : false, // Automatically parses the JSON string in the response
      transform: this.autoParse
    };
   
    let result = rp(options).promise()

    let timeBetweenRequests = parseInt(config.timeBetweenRequests) || 150
    await Client.timeout(timeBetweenRequests) // slow down for Genetec SDK since it cannot handle concurrency well

    return result
  }

  /*
    Send DELETE request
  
    @param {string} path
    @param {object} params

    @return {promise}
  */
  static async delete(path, params){
    let uri = URL.resolve(this.getBaseUrl(), path)
    uri += `?${this.genParamsString(params)}`
    console.log(`DELETE: ${uri}`)

    let options = {
      method: 'DELETE',
      uri: uri,
      auth: this.getAuth(),
      headers: null,
      json: process.env.NODE_ENV=='test' ? true : false // Automatically parses the JSON string in the response
    };

    let result = rp(options).promise()

    let timeBetweenRequests = parseInt(config.timeBetweenRequests) || 150
    await Client.timeout(timeBetweenRequests) // slow down for Genetec SDK since it cannot handle concurrency well
   
    return result
  }


  static timeout(ms) {
    console.log(`=== Wait | ${ms} ms`)
    return new Promise(resolve => setTimeout(resolve, ms));
  }

}

module.exports = Client
