'use strict';

const fs = require('fs-extra');
const pkg = require('../package.json');
const path = require('path');
const bunyan = require('bunyan');
const bformat = require('bunyan-format');
const RotatingFileStream = require('bunyan-rotating-file-stream');
const config = require('./config.js');

const LOGDIR = path.resolve(process.cwd(), 'logs');
const LOGPATH = path.resolve(LOGDIR, `${pkg.name}.log.json`);

try {
  fs.mkdirsSync(path.dirname(LOGPATH));
} catch (err) {
  console.log('ERROR: Could not create log directory!', err);
}

const formatConsole = bformat({ outputMode: 'short' });


//Initialize Bunyan
const log = bunyan.createLogger({
  name: pkg.name,
  serializers: {err: bunyan.stdSerializers.err}, // bunyan.stdSerializers,
  streams: []
});

log.addStream({
    name: 'file',
    type: 'raw',
    level: config.get("config:log:level"),
    stream: new RotatingFileStream({
        path: LOGPATH,
        period: config.get('config:log:period'),
        totalFiles: config.get('config:log:count'),
        rotateExisting: true,  // Give ourselves a clean file when we start up, based on period 
        threshold: config.get('config:log:threshold'),      // Rotate log files larger than 10 megabytes 
        totalSize: config.get('config:log:totalSize'),      // Don't keep more than 20mb of archived log files 
        gzip: config.get('config:log:gzip')
    })
});


//If log flag spacified, pipe Bunyan logs to stdout
if (config.get('log')) {
  log.addStream({
    name: 'console',
    stream: formatConsole,
    level: (typeof config.get('log') === 'string') ? config.get('log') : config.get("config:log:level")
  })
}

//TODO: Add file out method for debugging PowerShell and other IO operations
/*
log.file = function(level, filename, data) {
  //If level is >= config:log:level then write data to file under LOGDIR
}
*/

log.debug({
  config:config.get('config:log'),
  cli: config.get('log'),
  path: LOGPATH
}, "Logger Initialized.");

module.exports = log;