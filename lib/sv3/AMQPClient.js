/** AMQPClient

This is the SV3 Client over AMQP.

*/
const util = require('util');
const EventEmitter = require('events');
var amqp = require('amqp');
var log = require('../logger.js').child({module:'AMQPClient'});


/**
 * SV3 AMQP Client
 * @constructor
 */
function AMQPClient(){
	EventEmitter.call(this);
}
util.inherits(AMQPClient, EventEmitter);

/**
 * Connect to the remote server
 * @param {object} connectionOptions - AMQP connection options
 * @param {object} subPubOptions - sub pub queue exchange route options
 */
AMQPClient.prototype.connect = function(connectionOptions, subPubOptions){
	var self = this;
	this.options = connectionOptions;

	this.exchangeName = subPubOptions.exchangeName;
	this.subQueueName = subPubOptions.subQueueName;
	this.exchangeAutoDelete = subPubOptions.exchangeAutoDelete;
	// this.subRouteKey = subPubOptions.subRouteKey;
	// this.pubRouteKey = subPubOptions.pubRouteKey;

	log.debug({config: this.options}, "Connecting to AMQP");
	this.mq = amqp.createConnection(this.options);
	log.debug({config: subPubOptions}, "Connected to AMQP");

	this.mq.on('error', function(err){
		log.error({err: err}, 'AMQP error!');
	});

	this.mq.on('ready', function(){
		log.info('Connected!!');
		self.start();
	});

	this.mq.on('close', function(){
		log.warn('AMQP disconnected.');

		if (this.options.reconnect) {
			setTimeout(5000, self.reconnect);
		}
	});
}

/**
 * Attempt to reconnect automatically
 */
AMQPClient.prototype.reconnect = function(){
	this.connect(this.options);
}

/**
 * Gracefully shut down
 */
AMQPClient.prototype.stop = function(options){
	// Attempt disconnecting (again)
	try {
		this.mq.disconnect();
	} catch (err) {
		log.warn("Tried disconnecting but mq is already Dereference");
	}
	

	// Dereference the exchange
	this.exchange = null;

	// Delete the AMQP client
	delete this.mq;
}

/**
 * Subscribe to remote channels
 */
AMQPClient.prototype.start = function(options){
	var self = this;
	var mq = this.mq;

	// subscribe to sv3 instructions
	let exchangeOption = {durable: true, autoDelete: this.exchangeAutoDelete}
	// if(this.pubRouteKey == ''){
	// 	exchangeOption.type = 'fanout'
	// }

	this.exchange = mq.exchange(self.exchangeName, exchangeOption, function(){
		log.info({name: self.exchangeName}, 'Opened exchange');

		if(self.subQueueName){
			mq.queue(self.subQueueName, {
				durable: true,
				autoDelete: this.exchangeAutoDelete
			}, function(q) {
				log.debug({name: self.subQueueName}, 'Queue binded');

				// q.bind(self.exchangeName, self.subRouteKey);

				q.subscribe({ ack: true }, function(message) {
					//Emit all messages on the data event
					self.emit('data', message);

					//Acknowledge message
					//TODO: Move to callback, only called when command is completed and requeue if necessary.
					q.shift();
				});
			});
		}
	});

	this.on('data', function(message){
		log.trace({data: message}, 'Received Data');

		//Emit specific actions
		if (message.action) self.emit(message.action, message);
	});
}

AMQPClient.prototype.send = function(data, routeKey) {
	if (!this.exchange) throw new Error("Not connected!");

	log.debug({data:data, route:routeKey}, "Publishing...");
	this.exchange.publish(routeKey, data, {mandatory: true}, function(err){
		if (err) log.error({err:err}, "Sending failed.");
		else log.debug("Sending returned success");
	});
}


module.exports = AMQPClient;


/* Old code for reference

// connect to mq for instructions
var exchangeName = "ccure";
var subQueueName = "sp2ccure_" + nconf.get('app.code');
var subRouteKey = "sp2ccure_" + nconf.get('app.code');
var pubRouteKey = "ccure2sp";

var mq = null;

function connectToMQ() {
	log.debug("Connecting to AMQP", {config: nconf.get('mq')});

	mq = amqp.createConnection(nconf.get('mq'));

	mq.on('error', function(err) {
		log.error("mq error:" + err);
		setTimeout(function(){
			process.exit(1);
		}, 3000);
	});

	mq.on('close', function() {
		log.error("mq disconnected");
		setTimeout(function(){
			process.exit(1);
		}, 3000);
	});

	mq.on('ready', function() {
		log.debug('Connected to mq');
		// connect to ccure
		ccure.login({
			onSuccess: function() {
				// keep session alive
				if (TaskKeepSessionAlive) {
					clearInterval(TaskKeepSessionAlive);
					TaskKeepSessionAlive = null;
				}
				TaskKeepSessionAlive = setInterval(function() {
					ccure.login({});
				}, config.ccure.sessionRefreshInterval);

				// read card
				if(enableCardRead){
					if (TaskCardRead) {
						clearInterval(TaskCardRead);
						TaskCardRead = null;
					}
					TaskCardRead = setInterval(function() {
						var now = moment();
						var endDate = now.subtract(config.ccure.fetchCardEndFromNow, 'seconds');
						var endStr = endDate.format('MM/DD/YYYY HH:mm:ss');
						var startDate = endDate.subtract(config.ccure.fetchCardDuration, 'seconds');
						var startStr = startDate.format('MM/DD/YYYY HH:mm:ss');
						console.log("read:", startStr, endStr);
						ccure.findAllCardReads({
							fromDate: startStr,
							toDate: endStr,
							onSuccess: function(reads) {
								if (!_.isEmpty(reads)) {
									var r = {
										customer_code: config.app.code,
										data_type: "cardreads",
										data: reads
									};
									_.each(reads, function(i) {
										log.debug("card:", i.card);
									})
									socket.emit("reader:read", r);
								}
							}
						});
					}, config.ccure.fetchCardInteval);
				}

				// subscribe to sv3 instructions
				var exchange = mq.exchange(exchangeName, {
					durable: true
				}, function(){
					mq.queue(subQueueName, {
						durable: true,
						autoDelete: false
					}, function(q) {
						log.debug("-----queue binded-----");
						q.bind(exchangeName, subRouteKey);
						q.subscribe({ ack: true }, function(message) {
							try{
								// message = message.toString();
								log.debug(message);
								var json = message;
								if (json.action == 'fetch_directory') {
									ccure.fetchDirectory({
										onSuccess: function(data) {
											var r = {
												customer_code: config.app.code,
												data_type: "directory",
												data: data
											}
											exchange.publish(pubRouteKey, r);
										}
									});
								} else if (json.action == 'fetch_clearances') {
									ccure.findClearanceGroups({
										name: json.name,
										onSuccess: function(groups) {
											var g = groups[0];
											ccure.findClearancesForGroup({
												groupID: g.id,
												onSuccess: function(data) {
													var r = {
														customer_code: config.app.code,
														data_type: "clearances",
														data: data
													}
													exchange.publish(pubRouteKey, r);
												}
											});
										}
									});
								} else if (json.action == 'fetch_readers') {
									ccure.findDoors({
										onSuccess: function(data) {
											var r = {
												customer_code: config.app.code,
												data_type: "readers",
												data: data
											}
											exchange.publish(pubRouteKey, r);
										}
									});
								} else if (json.action == 'create_visitor') {
									var visitorObj = {
										firstName: json.first_name,
										lastName: json.last_name,
										cardNumber: json.card,
										activeDate: json.active_date,
										expiryDate: json.expiry_date,
										clearanceIDs: json.clearance_ids,
										hostPersonnelGuid: json.temp_badge_for_user_guid,
										onSuccess: function(data) {
											//log.debug(data);
										},
										onError: function(err) {
											log.debug(err);
										}
									}
									ccure.checkin(visitorObj);
								} else if (json.action == 'revoke_visitor') {
									var obj = {
										cardNumber: json.card,
										hostPersonnelGuid: json.temp_badge_for_user_guid
									};
									obj.onSuccess = function() {};
									ccure.checkout(obj);
								} else {
								}
							}
							catch(ex){
								log.error("err: ", ex);
							}
							finally {
								q.shift();
							}
						});
					});
				});
			},
			onError: function(body, res, err){
				log.error("ccure login error", err);
				process.exit(1);
			}
		});
	});
}

*/