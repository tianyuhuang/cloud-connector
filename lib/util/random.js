"use strict";

const names = require('node-random-name');
const uuid = require('uuid/v4');

const RAND = Math.random;

class Random {

	static cardNumber(min=10000, max=65000) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	static firstName() { 
		return names({random: RAND, first: true});
	}

	static lastName() { 
		return names({random: RAND, last: true});
	}

	static email() {
		return `${this.firstName()}.${this.firstName()}.${new Date().getTime()}@lvh.me`;
	}

	static uuid() {
		return uuid();
	}

	constructor(properties={}) {
		this.firstName = Random.firstName();
		this.lastName = Random.lastName();
		this.uuid = Random.uuid();
		this.cardNumber = Random.cardNumber();
		this.email = `${this.firstName}.${this.lastName}@lvh.me`;

		//Set/Override any properties specified
		Object.assign(this, properties);
	}

	get FirstName ()	{ return this.firstName }
	get LastName ()		{ return this.lastName }
	get EmailAddress ()	{ return this.email }
	get GUID ()			{ return this.uuid }
	get CardNumber ()	{ return this.cardNumber }
	get Name()			{ return `${this.LastName}, ${this.FirstName}` }
}

module.exports = Random;