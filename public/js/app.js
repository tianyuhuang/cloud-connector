

var dao = {
	fetch: function(namespace){
		return new Promise(function(resolve, reject){
			if ('app' === namespace) {
				resolve({app: { 
					code: 'e6745360-43e1-0135-872c-080027e22446', port: 7505 
				}})
			} else {
				resolve({});
			}
		})
	}
};

var _data = {
		app: {
			code: 'bla',
			visit_feature: true,
			log_level: 'DEBUG'
		}
	}

let conf = new Vue({
	data: _data,

	created() {
		dao.fetch('app').then(r => { _.merge(_data, r); console.log("merged", _data) });
	},
	updated() {
		console.log('updated', _data);
	}
});

conf.$mount('#configurator');
