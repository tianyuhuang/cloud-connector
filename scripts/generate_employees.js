'use strict'

require('dotenv').config()

const random = require('../lib/util/random.js');
const moment = require('moment');
const _ = require('lodash');

// Models
const Credential = require('../lib/ccure/Credential.js');
const Partition = require('../lib/ccure/Partition.js');
const Personnel = require('../lib/ccure/Personnel.js');
const PersonnelType = require('../lib/ccure/PersonnelType.js');
const Visit = require('../lib/ccure/Visit.js');

const Client = require('../lib/ccure/Client.js');
const Service = require('../lib/ccure/Service.js');

const client = new Client();
const service = new Service(client);


new Promise((resolve, reject) => {
	client.login((err, token) => {
		if (err) reject(err);
		else resolve(token);
	});
})
.then(() => {
	return service.find(PersonnelType, {Name: 'Employee'})
}).then(pType => {
	if (pType.length > 0) {
		return Promise.all(
			Array.from( new Array(300), (n,i) => {
				let p = new Personnel(new random());
				p.PersonnelTypeID = pType[0].ObjectID
				return service.save(p); 
			})
		);
	} else {
		throw new Error('Employee PersonnelType Not Found!');
	}
}).catch(err => {
	//Probably an uncaught ESOCKETTIMEDOUT due to overloading IIS.
	//TODO: Investigate further. Are we reaching the server's socket connection limit (probably)
	//or is this a Node.js limit?  Try rate limiting?
	console.log("Error!", err);
});