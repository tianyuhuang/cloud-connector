/**
Main server.js
 
This is the main service class.  It's aim is to:

* Initialize license and configuration
* Connect to SV3 using the MQ client
* Connect to CCURE9000 using the REST/WebSocket client
* Broker messages between the services

*/


const nconf = require('./lib/config.js');
const moment = require('moment');
const xml2js = require('xml2js');

const CCURE = require('./ccure.js');
const Victor = require('./victor.js');

const log = require('./lib/logger.js');
const AMQPClient = require('./lib/sv3/AMQPClient.js');
const CcureKeepAlive = require('./lib/ccure/KeepAlive.js');
const CcureClient = require('./lib/ccure/Client.js');
const CcureService = require('./lib/ccure/Service.js');
const Credential = require('./lib/ccure/Credential.js');



class CCUREStarter{
	static start(){

		// Connect to C*CURE9000
		const ccure = new CCURE(nconf.get('ccure:connection'));	//Instance of depricated CCURE client/service
		const client = new CcureClient(nconf.get("ccure:connection"));
		const service = new CcureService(client);
		const keepalive = new CcureKeepAlive(client, nconf.get('ccure:connection:keepalive'), false);

		let oldkeepalive = null;
		//Initiate first login for old/depricated client
		ccure.login();
		//Create keepalive loop for old/depricated client
		oldkeepalive = setInterval(function(){
			log.trace({version: 'CCURE lib v1.0'}, "Keepalive calling login()")
			ccure.login()
		}, nconf.get('ccure:connection:keepalive'));


		// get config
		let appCode = nconf.get('app:code')
    let mqConfig = nconf.get('mq')
		let pubRouteKey = 'cc-sv3visitor'
		let pubsubOptions = {
			exchangeName: mqConfig.defaultExchangeName,
			subQueueName: `sp2ccure_${appCode}`
		}


		// connect mq
		let sv3Client = new AMQPClient()
		client.login(function(err) {
			if (err) {
				log.error('Login failure.  Exiting.');
				console.log("CCURE9000 Login Failure.  Exiting.");
				process.exit(1);
			} else {
				keepalive.start();

				// Connect to SV3 / RabbitMQ
				//TODO: connect independently of CCURE login; hold requests until successful login
				sv3Client.connect(mqConfig, pubsubOptions);
			}
		});


		//TODO: begin polling card reads from Journal


		sv3Client.on('fetch_directory', function(json){
			log.debug("Received %s command. Trying...", json.action);
			try {
				ccure.fetchDirectory({
					onSuccess: function(data) {
						log.debug({data:data}, "fetchDirectory: returned (called success handler). Pushing to sv3Client");
						sv3Client.send({
							customer_code: appCode,
							data_type: "directory",
							data: data
						}, pubRouteKey);
					}
				});
			} catch (err) {
				log.error({err: err}, "Failed to Fetch Directory!");
			}
		});

		sv3Client.on('fetch_credentials', function(json){
			log.info('Received fetch_credentials command');
			service.findAll(Credential, {})
			.then(cards => {
				log.info({count:cards.length}, 'Returning Credentials')
				sv3Client.send({
					customer_code: appCode,
					data_type: 'credentials',
					data: cards
				}, pubRouteKey)
			})
			.catch(err => {
				log.error({err}, "Error getting credentials!");
				sv3Client.send({
					customer_code: appCode,
					data_type: 'error',
					data: err
				}, pubRouteKey)
			})
		})


		sv3Client.on('fetch_clearances', function(json){
			try {
				ccure.findClearanceGroups({
					name: json.name,
					onSuccess: function(groups) {
						var g = groups[0];
						ccure.findClearancesForGroup({
							groupID: g.id,
							onSuccess: function(data) {
								sv3Client.send({
									customer_code: appCode,
									data_type: "clearances",
									data: data
								}, pubRouteKey);
							}
						});
					}
				});
			} catch (err) {
				log.error({err: err}, "Failed to Fetch Clearances!");
			}
		});

		sv3Client.on('fetch_readers', function(json){
			try {
				ccure.findDoors({
					onSuccess: function(data) {
						sv3Client.send({
							customer_code: appCode,
							data_type: "readers",
							data: data
						}, pubRouteKey);
					}
				});
			} catch (err) {
				log.error({err: err}, "Failed to Fetch Readers!");
			}
		});


		sv3Client.on('create_visitor', function(json){
			//TODO: check to see if this is a temp badge for employee.  If yes, use old ccure.js library.
			//TODO: implement temp badge issuance in new CcureService
			//TODO: Message should be transactional: only confirm if checkin is successful!
			service.checkin(json)
			.then(result => {
				log.info({
					visitor: { 
						card: json.card, 
						name: `${json.last_name}, ${json.first_name}`,
						guid: json.guid
					},
					host: json.host
				}, "Check-in completed");
			})
			.catch(err => {
				log.error({json, err}, "Error completing create_visitor command!");
			});
		});

		sv3Client.on('revoke_visitor', function(json){
			try {
				ccure.checkout({
					cardNumber: json.card,
					hostPersonnelGuid: json.temp_badge_for_user_guid,
					onSuccess: function(data) {}
				});
			} catch (err) {
				log.error({err: err}, "Failed to Revoke Visitor!");
			}
		});

		sv3Client.on('exit', function(json){
			log.info('Exit command received. Exiting in 3s.');

			//Exit in later cycle.  Must ack message or exit command will remain in queue when restarted.
			setTimeout(()=> {
				log.info('Exiting.');
				process.exit(0);
			}, 3000)
		});


		if (nconf.get('ccure:polling:enabled')) {
			service.journal.subscribe('SoftwareHouse.NextGen.Common.LogMessageFormats.CardAdmitted');
			service.journal.subscribe('SoftwareHouse.NextGen.Common.LogMessageFormats.CardRejected');
			service.journal.on('event', (data) => {
				if ('CardAdmitted' === data.MessageType || 'CardRejected' === data.MessageType) {
					xml2js.parseString(data.XmlMessage, (err, result) => {
						
						let read = {
							guid: data.GUID,
							message_type: data.MessageType,
							utc: data.MessageUTC,
							card: result.LogMessage.Card || result.LogMessage.CHUID,
							location_guid: data.SecondaryObjectIdentity,
							location_name: data.SecondaryObjectName
						};
						log.debug({read}, "Emit card read");
						socket.emit('reader:read', {
							customer_code: nconf.get('app:code'),
							data_type: "cardreads",
							data: [read]
						})
					})
				}
			})
		}

		console.log("Server started.");
		log.info("Server started");
	}
}

/*
          mq.on('ready', function() {
            log.debug('Connected to mq');
            // connect to ccure
            ccure.login({
              onSuccess: function() {
                // keep session alive
                if (TaskKeepSessionAlive) {
                  clearInterval(TaskKeepSessionAlive);
                  TaskKeepSessionAlive = null;
                }
                TaskKeepSessionAlive = setInterval(function() {
                  ccure.login({});
                }, config.ccure.sessionRefreshInterval);

                // read card
                if(enableCardRead){
                  if (TaskCardRead) {
                    clearInterval(TaskCardRead);
                    TaskCardRead = null;
                  }
                  TaskCardRead = setInterval(function() {
                    var now = moment();
                    var endDate = now.subtract(config.ccure.fetchCardEndFromNow, 'seconds');
                    var endStr = endDate.format('MM/DD/YYYY HH:mm:ss');
                    var startDate = endDate.subtract(config.ccure.fetchCardDuration, 'seconds');
                    var startStr = startDate.format('MM/DD/YYYY HH:mm:ss');
                    console.log("read:", startStr, endStr);
                    ccure.findAllCardReads({
                      fromDate: startStr,
                      toDate: endStr,
                      onSuccess: function(reads) {
                        if (!_.isEmpty(reads)) {
                          var r = {
                            customer_code: config.app.code,
                            data_type: "cardreads",
                            data: reads
                          };
                          _.each(reads, function(i) {
                            log.debug("card:", i.card);
                          })
                          socket.emit("reader:read", r);
                        }
                      }
                    });
                  }, config.ccure.fetchCardInteval);
                }


*/



/*

				try{

					var json = message;
					if (json.action == 'fetch_directory') {
						ccure.fetchDirectory({
							onSuccess: function(data) {
								var r = {
									customer_code: config.app.code,
									data_type: "directory",
									data: data
								}
								exchange.publish(pub_route_key, r);
							}
						});
					} else if (json.action == 'fetch_clearances') {
						ccure.findClearanceGroups({
							name: json.name,
							onSuccess: function(groups) {
								var g = groups[0];
								ccure.findClearancesForGroup({
									groupID: g.id,
									onSuccess: function(data) {
										var r = {
											customer_code: config.app.code,
											data_type: "clearances",
											data: data
										}
										exchange.publish(pub_route_key, r);
									}
								});
							}
						});
					} else if (json.action == 'fetch_readers') {
						ccure.findDoors({
							onSuccess: function(data) {
								var r = {
									customer_code: config.app.code,
									data_type: "readers",
									data: data
								}
								exchange.publish(pub_route_key, r);
							}
						});
					} else if (json.action == 'create_visitor') {
						var visitorObj = {
							firstName: json.first_name,
							lastName: json.last_name,
							cardNumber: json.card,
							activeDate: json.active_date,
							expiryDate: json.expiry_date,
							clearanceIDs: json.clearance_ids,
							hostPersonnelGuid: json.temp_badge_for_user_guid,
							onSuccess: function(data) {
								//log.debug(data);
							},
							onError: function(err) {
								log.debug(err);
							}
						}
						ccure.checkin(visitorObj);
					} else if (json.action == 'revoke_visitor') {
						var obj = {
							cardNumber: json.card,
							hostPersonnelGuid: json.temp_badge_for_user_guid
						};
						obj.onSuccess = function() {};
						ccure.checkout(obj);
					} else {
					}
				}
				catch(ex){
					log.error("err: ", ex);
				}
				finally {
					q.shift();
				}
			});
		});
	});
}


*/



//TODO: catch requests to shutdown and exit gracefully with a goodbye

module.exports = CCUREStarter