const nconf = require('./lib/config.js')
const log = require('./lib/logger.js')
const AMQPClient = require('./lib/sv3/AMQPClient.js')
const CustomField = require('./lib/genetec/entity/CustomField')
const Cardholder = require('./lib/genetec/entity/Cardholder')
const CardholderGroup = require('./lib/genetec/entity/CardholderGroup')
const Visitor = require('./lib/genetec/entity/Visitor')
const LPRMonitor = require('./lib/genetec/lib/LPRMonitor')
const Entity = require('./lib/genetec/entity/Entity')

class GenetecStarter {
  static async start(){
    // get config
    let appCode = nconf.get('app:code')
    let mqConfig = nconf.get('mq')
    let pubsubOptions = {
      exchangeName: mqConfig.defaultExchangeName,
      subQueueName: `sv3-cc-${appCode}`,
      exchangeAutoDelete: mqConfig.autoDelete
    }

    // connect mq
    let sv3Client = new AMQPClient()
    sv3Client.connect(mqConfig, pubsubOptions)

    // vehicle cc
    let lprEnabled = nconf.get('genetec:lprSDK:enabled')
    if(lprEnabled){
      // get config
      let lprDir = nconf.get('genetec:lprSDK:lprDir')
      let pubRouteKey = `cc-sv3vehicle-${appCode}`

      // watch dir      
      // this.watchLPRDir(lprDir, sv3VehicleClient)
      let lprMonitor = new LPRMonitor({dir: lprDir})
      lprMonitor.start()
      lprMonitor.on("lpr-read-detected", (json) => {
        sv3Client.send(json, pubRouteKey)
      })
    }

    // visitor cc
    let visitorEnabled = nconf.get('genetec:webSDK:enabled')
    if(visitorEnabled){

      // setup dependencies
      let createCustomField = false
      if(createCustomField){
        try{
          console.log("Create Custom Fields")
          await CustomField.createFieldForEntity("CardHolderGroup", {"name": "Floor", "type": "Text"})
        }catch(err){}
      }
      
      //
      let pubRouteKey = 'cc-sv3visitor'

      //
      sv3Client.on('fetch_directory', async (json) => {
        log.debug("Received %s command. Trying...", json.action)
        try {

          // do not inlcude members, it takes too long when too many members
          let groups = await CardholderGroup.findAllGroups({isVisitorGroup:false, includeMembers:false, includeMemberIDs:true})
          log.trace({data: groups}, "fetchDirectory: returned (called success handler). Pushing to sv3Client")
          console.log("=== Directory | found groups: ", groups.length)

          let fieldCompanyName = nconf.get('genetec:cardholder:fieldCompanyName')
          let fieldCompanyFloor = nconf.get('genetec:cardholder:fieldCompanyFloor')

          for(var i in groups){
            // reset resJson
            let resJson = {
              personnels: [],
              groups: [],
              pairs: []
            }

            // get group
            let g = groups[i]
            let groupData = {
              guid: g.guid,
              name: g.name,
              floor: g.floor
            }

            // get members
            if(g.memberIDs){
              console.log(`=== Directory | found members for group: ${g.name}, total: ${g.memberIDs.length}`)

              for(var iMember in g.memberIDs){
                let memberID = g.memberIDs[iMember]
          
                try{
                  let member = await Cardholder.getSingleByGuid(memberID)

                  if(member){
                    let m = member

                    // get group
                    resJson.groups = []
                    let group = {
                      guid: groupData.guid,
                      name: groupData.name,
                      floor: groupData.floor
                    }
                    if(fieldCompanyName == "Cardholder.Description") {
                      group.guid = null
                      group.name = m.companyName
                    }
                    if(fieldCompanyFloor == "Cardholder.CustomFields.Floor") {
                      group.floor = m.floor
                    }
                    resJson.groups.push(group)

                    // get personnel
                    resJson.personnels = []
                    let personnel = {
                      guid: m.guid,
                      first_name: m.firstName,
                      last_name: m.lastName,
                      email: m.email,
                      group_name: group.name,
                      group_guid: group.guid,
                      group_floor: group.floor
                    }
                    resJson.personnels.push(personnel)

                    // get pair
                    resJson.pairs = []
                    let pair = {
                      personnel_guids: [memberID],
                      group_guid: group.guid
                    }
                    resJson.pairs.push(pair)

                    console.log("=== Directory | push member to queue: ", resJson.personnels[0])
                    // send one user at a time
                    sv3Client.send({
                      customer_code: nconf.get('app:code'),
                      data_type: "directory",
                      data: resJson
                    }, pubRouteKey)

                  }
                }
                catch(err){
                  console.log(`=== Error: fetch directory for memberID: ${memberID}`)
                }

              }
            }
          }
          
        } catch (err) {
          console.log("=== Error: Failed to Fetch Directory!", err)
          log.error({err: err}, "Failed to Fetch Directory!");
        }
      })

      //
      sv3Client.on('fetch_clearances', async (json) => {
        let groups = await CardholderGroup.findAllGroups({isVisitorGroup:true})
        console.log("=== Clearances | found total: ", groups.length)

        let groups_data = groups.map((g)=>{
          return {
            id: g.id,
            guid: g.guid,
            name: g.name,
            floor: g.floor
          }
        })
        sv3Client.send({
          customer_code: nconf.get('app:code'),
          data_type: "clearances",
          data: groups_data
        }, pubRouteKey)
        
      })

      //
      sv3Client.on('create_visitor', (json) => {
        Visitor.checkIn(json)
      })

      //
      sv3Client.on('revoke_visitor', (json) => {
        Visitor.checkOut({card: json.card}).then(r => {
          log.info({visitor: json}, "Check-out completed")
        })
      });
      
    }
    
  }

  

}

module.exports = GenetecStarter