const nconf = require('./lib/config.js');

let ccureEnabled = nconf.get('ccure:enabled');
let genetecEnabled = nconf.get('genetec:enabled');

if(ccureEnabled){
  const CCUREStarter = require('./server-ccure')
  CCUREStarter.start()
}

if(genetecEnabled){
  const GenetecStarter = require('./server-genetec')
  GenetecStarter.start()
}