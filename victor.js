var child_procss = require('child_process');
var log = require('./lib/logger.js');

Victor = {

	//TODO: Why is it necessary to recycle victor?  Under what conditions must this be done?
	reload: function(){
		var cmd = "\"%windir%\\System32\\inetsrv\\appcmd.exe\" recycle apppool /apppool.name:\"victorwebservice\"";
		try{
			child_procss.execSync(cmd);
		} catch (ex) {
			//command not found or error running.
			log.error({err: ex}, "Error recycling victorwebservice");
		}
	}
}

module.exports = Victor;